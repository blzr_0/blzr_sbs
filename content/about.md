---
title : "About BLZR"
date : 2023-11-15T16:09:43+05:30
draft : false
scrolltotop : false
custom_css : 
    - "about/about.scss"
---

```markdown
Hello, friend. Hello, friend!? That's lame.
Maybe I should give you a name...
```

### find . -type f -name "BLZR" -exec cat {} \\;

1BL4Z3R is my GitHub username. It was autumn of 2004 and I just had a revealing experience after reading "I,Robot"; which acted as an inspiration for the username. Learning from an early age that one shouldn't use their real identity, I adopted this username to look cool like my peers. It should be stylized as “I,BL4Z3R”, but I ran out of budget for that. Since then, it struck like that.

Now, I have adopted BLZR by truncating `\d`. I think my creativity is on-par with Apple.

### grep /$(id -u -n): /etc/passwd

Permit me then, in lieu of the more commonplace sobriquet, to suggest the character of this dramatis persona.

{{< figure src="images/about/smile-rectangle_128.png" class="right" width="128" height="128" >}}

My name is Ristik, and I am working as a DevOps engineer, with an expertise is in cloud solutions, where I have worked extensively in infrastructure and network deployments. This blog is sort of a side project of mine where I publish everything I learn about and slowly build a map (I could do the same in Obsidian, but where's the fun in that?)

Being an inquisitive person, I sort of love reading, researching, and learning about industry trends, best practices, and technology stacks, primarily in the domain of cloud computing.

If you are interested in learning more from me, working with me, or employing me in your organisation, use the [contact page]({{< relref "contact" >}}) or get hold of me through the social media handles mentioned below.

### lsof -u $(id -u -n)

I found many project has [`/uses` trivia](https://github.com/wesbos/awesome-uses). So, I have also decided to follow suit and see where I end up in. Here we go. Since I don't have much to share, I believe a new page would be ridiculus. :man_technologist:

Hardware :

* :computer: HP Pavilion x360 - 15g-br019tx
* :iphone: Galaxy M35
* 🖥 RaspberryPi 3B+

Software :

* OS - 
    * :computer:[ Fedora Linux](https://fedoraproject.org/)
    * :iphone: Android ([OneUI](https://www.samsung.com/us/apps/one-ui/))
    * 🖥 [DietPi](https://dietpi.com/) 
* Code Editor/ IDE - [VSCodium](https://vscodium.com)
* Terminal - [Console](https://apps.gnome.org/Console/)
* Shell - [BASH](https://www.gnu.org/software/bash)
* Markdown Editor - [Joplin](https://joplinapp.org/)
* Browser - 
    * :computer: [Firefox](https://www.mozilla.org/en-US/firefox/new)
    * :computer: [LibreWolf](https://librewolf.net)
    * :computer: [Brave](https://brave.com/)
    * :computer: [Tor Browser](https://www.torproject.org)  
    * :iphone: [Mull](https://github.com/divested-mobile/mull-fenix) Privacy oriented and deblobbed web browser based on Firefox
    * :iphone: [Cromite](https://github.com/uazo/cromite) Chromium fork based on Bromite + ad blocking and privacy
* Notes/PKM - [Joplin](https://joplinapp.org/) Privacy-first, open-source note-taking app
* Misc -
    * [Pinta](https://www.pinta-project.com) : FOSS for drawing and image editing
    * [qBittorrent](https://www.qbittorrent.org) : OSS alternative to µTorrent
    * [Stremio](https://www.stremio.com) : Modern media center that gives the freedom to watch anything
    * [Localsend](https://localsend.org) : OSS & cross-platform way to share files to nearby devices
    * [NewPipe](https://newpipe.net) : The lightweight YouTube experience for Android
* Self Hosted -
   * [Nginx Proxy Manager](https://nginxproxymanager.com) : Reverse Proxy services with SSL
   * [Syncthing](https://syncthing.net) : FOSS continuous file synchronization program
   * [Photoprism](https://www.photoprism.app) : AI-Powered Photos App for the Decentralized Web
   * [File Browser](https://filebrowser.org) : WebUI for file managing interface within a specified directory
   * [Vaultwarden](https://github.com/dani-garcia/vaultwarden) : Alternative implementation of the Bitwarden server API written in Rust
   * [PiHole](https://pi-hole.net) : DNS sinkhole for network-wide ad blocking in (mostly) local network
   * [Homer](https://github.com/bastienwirtz/homer) : Simple static HOMepage for servER for services from a yaml file
   * [Docker Firefox](https://github.com/jlesage/docker-firefox) : Docker container for Firefox
   * [Davis](https://github.com/tchapi/davis) : Simple, fully translatable admin interface and frontend for sabre/dav

Games I have been playing
   * Minecraft
   * Eve Online
   * Watch Dogs

### echo "lecture = always"

```markdown
We trust you have received the usual lecture from BLZR. 
It usually boils down to these three things:

    #1) Thoughts expressed here are 
        a. my own, 
        b. mostly pointless and 
        c. often silly
    #2) Think before you speak or type, words can hurt
    #3) With great power, there must also come -- great responsibility
```

You are still reading?? I can't believe it... :partying_face: