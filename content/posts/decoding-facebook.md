﻿---
title : 'Decoding Facebook'
slug : 'decoding-facebook'
tags: 
  - Personal
  - Privacy
  - Security
  - Random Access Memories
description : 'Facebook (Now Meta) loves you, so does the social engineers, for you being so naive'
date : 2018-02-27
toc : false
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---
> **This post appeared in NoPurposeClub on February 27, 2018.** This is the beauty of taking backups...

Facebook loves you, so does the social engineers, for you being so naive…

If you are here, to learn how Facebook works, then you are click-baited. This article is to learn how you work on Facebook (_Like as a datapoint, and neither as an employee, nor Works In FaceBook kinda scam_)

Facebook had turned to a social media giant, and many companies rely solely on it for their businesses, and also the meme-makers to gain popularity. But it is also a heaven of data for social engineers, if we forget for a second that FaceBook is running of from  your data.

But how does your profile can spit out plethora of information?
Let’s dig into it

First and foremost, your profile picture, where you are posing with your friends and even with your girl/boy friend and using image editing tools to enhance your beauty. Facebook uses your profile image and does face recognition analysis, that is why you get auto-tagged when a friend of yours clicks a groupie with you and posts on the platform.

For social engineers,  it helps them to identify you and your potential boy/girl friend. Your profile image shows up whenever someone searches for you on google images(try it!) It can also led to having your image superimposed on some fake identity card, and you end up getting caught for some illegal trade, that you never knew existed!
Speaking of your face, next is dedicated to those selfie-craze-people, who click and upload their filtered face, from different angles.

If you are using FaceID (iPhone fanboys/girls), some underground forums claims that, they can make 3-D print of your face, and use it to unlock your iPhone X, if they have access to some 10-12 photos of your face, and for video, 1 video is enough. Now for Hi-res selfies fans out there, you rarely scale down image resolution of your selfies and post the 1080p image. Recently, some groups had bypassed retina scan of Samsung phones, by only using hi-res images of the owner.

Now, comes the part where you gladly enter your school, your city, and sometimes, your residential address (Few “Angels” out there does so), you are under a severe threat, as the engineers can backtrack you to your location, given adequate images to locate the background, where you had done your clicking.
Using your phone number as your username is a serious vulnerability, as it not only allows our “Big Brother” to know where you are, apart from Google’s Location Tracking, via cell tower triangulation, but anyone having your number can be 50% closer to gaining access to your account, rest 50% being the password, as Facebook don’t warn about the different device used to login _by default_, while logging-in.

Now, you are addicted to signing up for various bots, that show who will be your future girlfriend ;), or who is secret admirer, and you happily give your credentials, to see randomly generated girlfriend is posted to your Feed, and you are not giving a second thought, what it does in background. Apart from handful of developers, who do it for fun, many develop those to gain your credentials, and later use them for various purposes, like spreading botnets, selling your messenger chats (which you never care to delete), or targeting your thousands of your friends to give up their credentials, for something that is remotely amusing.

The relationships that you make is a severe threat, as, social engineers know who is your parents, sisters, brothers etc. and can use them to get access to you, even if you checked out our previous points. This can expose not only you, but also you near and dear ones, to serious vulnerabilities. Also, the “Married, Single, Committed and Its Complicated” relationship tags that you had set up, will open you the targeted attacks, as the social engineers know where you stand in your personal life, and every data is gold not only to Facebook, also for the stalkers that monitor you.

Finishing off with your cringeworthy posts- It can give a great insight to what happens with you in everyday life, and what happened to you in past, as all is saved. The threat is severe, because it is like wearing a giant “Target Me” banner on your back.

As, more and more people are connecting on the platform, more data is being generated for Facebook to use (and, this we can’t help), and more target user base is generated for the social engineers, and all the profiles they have currently access to, “Doomsday Protocol” can be easily started.

I finish off with these lines- your security lies in your hands, if you want to be digitally open, you are making yourself open in real world too. We cannot be secure, or have complete privacy, but, what we can have is limited openness in our digital files, to make ourselves somewhat secure than the rest of the crowd. Making yourself secure and private, will ripple down to your near and dear ones, against the coming onslaughts.

---
COMMENTS


>heyyyy!!!!!!!
i nominated u for versatile award
https://writingpoetry53260872.wordpress.com/2018/02/26/versatile-blogger-award-yay-me/ - universe fireflie (February 27, 2018 at 3:50 pm)
>>Thank you for the nomination.I will look into it shortly. -bl4z3r (February 28, 2018 at 7:36 pm)

>kay :slightly_smiling_face: - universe fireflie(February 28, 2018 at 7:43 pm)
>>Assuming that it is “okay”; thankyou. - bl4z3r (February 28, 2018 at 8:05 pm)

>lol (laugh out loud) Yes It’s okay :slightly_smiling_face: - universe fireflie (February 28, 2018 at 9:06 pm)
>>Thanks again, for laughter, as it is the best medicine known to mankind. - bl4z3r (February 28, 2018 at 10:22 pm)

>yeah i love humor!! - universe fireflie (February 28, 2018 at 10:36 pm)
>>Highfive :raised_hand: - bl4z3r (March 1, 2018 at 11:29 am)
>high five - universe fireflie (March 1, 2018 at 3:28 pm)