---
title : 'Crash Code Part-3 (Word shortening with duplicates)'
slug : 'crash-code-3'
tags :
    - Personal
    - Code
description : 'Trying to solve old coding questions, because reasons'
date : 2023-01-02
toc : false
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---

> Following question was asked in `IBM Corporation-Campus Hiring- 18-Jan-23_B2`. Test duration: 30 mins

### Problem Description

A particular application uses the following logic to reduce length of sentences by shortening the individual words: 

- Take each word and remove the vowels in it to get its shortened form
- It is possible that two different words have the same shortened form. In this case:
    - The first shortened word is used as-is
    - For the second shortened word that is same as the first, number 1 is appended at the end
    - For the third shortened word that is same as the first, number 2 is appended at the end and so on.

Given a sentence, the program should print the final shortened version of the sentence based on the rules above.

For example, short form for both 'build and 'bold' would be 'bid' as per the rules above. The numbers to be added will depend on the order of the words in the sentence

{{< admonition type=note title="Consider this sentence:" >}}

```markdown
it was bold to make the build larger
```

In shortened version, 'bold' will be 'bid', while 'build' will be 'bld1'

Similarly, shortened versions of 'it and 'to' are the same. Based on the order of appearance, 'it' would be 't' and 'to' would be 'tl'

The output would be: 

```markdown
t ws bld t1 mk th bld1 lrgr
```
{{< /admonition >}}

#### Function Description: 

Complete the function shorten5entence in the editor below. The function must print the final shortened version of the sentence based on the rules above

shorten5entence has the following parameter(s): 
sentence: a String

### Constraints 
- Only lowercase letters would be present in the sentence 

### Examples 
#### Sample Case 0

Sample Input For Custom Testing 

```markdown
it was bold to make the build larger
```

Sample Output

```markdown
t ws bld t1 mk th bld1 lrgr
```

Explanation: 

The words are shortened by removing the vowels from each word. When there are repeating words in shortened form, the first one is used as-is and the second one onwards a number is appended. Thus, 'it' becomes 't','to' becomes t1'. Similarly 'bold' becomes 'bld' and 'build' becomes 'bld1'.

#### Sample Case 1
Sample Input For Custom Testing

```markdown
we made the builds larger
```   

Sample Output

```markdown
w md th blds lrgr
```

Explanation:

The words are shortened by removing the vowels from each word. Here all shortened versions are distinct, hence no need to append numbers. 

### Solve

{{< highlight python "linenos=table" >}}
#!/bin/python3
#
# Complete the 'shortenSentence' function below.
#
# The function accepts STRING sentence as parameter.
#
import re
def shortenSentence(sentence):

  # Write your code here
  
    arr = sentence.split()
    arr2 = []
    vowels = ['a', 'e', 'i', 'o', 'u']
    newwrd = ""
    count = 0
    for word in arr:
        for i in range (len(word)):
            if (word[i] not in vowels):
                newwrd = newwrd + word[i]
        if (newwrd in arr2):
            res = re.compile(newwrd)
            y = list(filter(res.match, arr2))[-1]
            if (y[-1].isnumeric()):
                newwrd = newwrd + str(int(y[-1]) + 1)
            else:
                newwrd = newwrd + str(1)
        arr2.append(newwrd)
        newwrd = ""
    print(" ".join(arr2))

if __name__ == '__main__': 
  [...]
{{< / highlight >}}