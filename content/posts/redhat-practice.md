---
title : 'Redhat Practice'
slug : 'redhat-practice'
tags :
    - Personal
    - Thoughts
    - Random Access Memories
    - Resource
description : 'Redhat Practice'
date : 2024-04-01
toc : false
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---
### QUESTION 1
Configure your Host Name, IP Address, Gateway and DNS.

Host name: station.domain40.example.com
/etc/sysconfig/network
hostname=abc.com
hostname abc.com
IP Address:172.24.40.40/24
Gateway:172.24.40.1
DNS:172.24.40.1

```bash
cd /etc/syscofig/network-scripts/
ls
vim ifcfg-eth0 #(Configure IP Address, Gateway and DNS) 
	IPADDR=172.24.40.40
	GATEWAY=172.24.40.1
	DNS1=172.24.40.1
vim /etc/sysconfig/network #(Configure Host Name)
	HOSTNAME= station.domain40.example.com
```

### QUESTION 2
Add 3 users: harry, natasha, tom.
The requirements: The Additional group of the two users: harry, Natasha is the admin group.
The user: tom's login shell should be non-interactive.

```bash
useradd -G admin harry
useradd -G admin natasha
useradd -s /sbin/nologin tom
id harry;id Natasha (Show additional group)
cat /etc/passwd (Show the login shell)
```
OR
```bash
system-config-users
```

### QUESTION 3
Create a catalog under /home named admins. Its respective group is requested to be the admin group. The group users could read and write, while other users are not allowed to access it.
The files created by users from the same group should also be the admin group.

```bash
cd /home/
mkdir admins /
chown .admin admins/
chmod 770 admins/
chmod g+s admins/
```

### QUESTION 4
Configure a task: plan to run echo hello command at 14:23 every day.

```bash
which echo
crontab -e
    23 14 * * * /bin/echo hello
crontab -l #(Verify)
```

### QUESTION 5
Create a 2G swap partition which take effect automatically at boot-start, and it should not affect the original swap partition.

```bash
fdisk /dev/sda
    p #(check Partition table)
    n #(create new partition: press e to create extended partition, press p to create the main partition, and the extended partition is further divided into logical partitions)
    Enter
    +2G
    t
    l
    W
partx -a /dev/sda
partprobe
mkswap /dev/sda8
Copy UUID
swapon -a
vim /etc/fstab
    UUID=XXXXX swap swap defaults 0 0
swapon -s
```

### QUESTION 6
Create a user named alex, and the user id should be 1234, and the password should be alex111.

```bash
useradd -u 1234 alex
passwd alex
    alex111
    alex111
```
OR
```bash
echo alex111|passwd -stdin alex
```

### QUESTION 7
Install a FTP server, and request to anonymous download from /var/ftp/pub catalog. (it needs you to configure yum direct to the already existing file server)

```bash
cd /etc/yum.repos.d
vim local.repo
    [local]
    name=local.repo
    baseurl=file:///mnt
    enabled=1
    gpgcheck=0
yum makecache
yum install -y vsftpd
service vsftpd restart
chkconfig vsftpd on
chkconfig --list vsftpd
vim /etc/vsftpd/vsftpd.conf
    anonymous_enable=YES
```

### QUESTION 8
Configure a HTTP server, which can be accessed through http://station.domain40.example.com.
Please download the released page from http://ip/dir/example.html.

```bash
yum install -y httpd
chkconfig httpd on
cd /var/www/html
wget http://ip/dir/example.html
cp example.com index.html
vim /etc/httpd/conf/httpd.conf
    NameVirtualHost 192.168.0.254:80
    <VirtualHost 192.168.0.254:80>
    DocumentRoot /var/www/html/
    ServerName station.domain40.example.com
    </VirtualHost>
```

### QUESTION 9
Configure the verification mode of your host account and the password as LDAP. And it can ldapuser40. The password is set as "password". And the certificate login successfully through
can be downloaded from http://ip/dir/ldap.crt. After the user logs on , the user has no host directory unless you configure the autofs in the following questions.

```bash
system-config-authentication
    LDAP Server: ldap//instructor.example.com (In domain form, not write IP)
```
OR
```bash
yum groupinstall directory-client (1.krb5-workstation 2.pam-krb5 3.sssd)
system-config-authentication
    1.User Account Database: LDAP
    2.LDAP Search Base DN: dc=example,dc=com
    3.LDAP Server: ldap://instructor.example.com (In domain form, not write IP) 4.Download CA Certificate
    5.Authentication Method: LDAP password
    6.Apply
getent passwd ldapuser40
```

### QUESTION 10
Configure autofs to make sure after login successfully, it has the home directory autofs, which is shared as /rhome/ldapuser40 at the ip: 172.24.40.10. and it also requires that, other ldap users can use the home directory normally.

```bash
chkconfig autofs on
cd /etc/
vim /etc/auto.master
    /rhome /etc/auto.ldap
cp auto.misc auto.ldap
vim auto.ladp
    ldapuser40 -rw,soft,intr 172.24.40.10:/rhome/ldapuser40
    * -rw,soft,intr 172.16.40.10:/rhome/&
service autofs stop
server autofs start
showmount -e 172.24.40.10
su - ladpuser40
```

### QUESTION 11
Configure the system synchronous as 172.24.40.10.

```bash
system-config-date
```

### QUESTION 12
Change the logical volume capacity named vo from 190M to 300M. and the size of the floating range should set between 280 and 320. (This logical volume has been mounted in advance.)

```bash
vgdisplay #(Check the capacity of vg, if the capacity is not enough, need to create pv , vgextend , lvextend)
lvdisplay #(Check lv)
lvextend -L +110M /dev/vg2/lv2
resize2fs /dev/vg2/lv2
mount -a #(Verify)
#-------------- (Decrease lvm)
umount /media
fsck -f /dev/vg2/lv2
resize2fs -f /dev/vg2/lv2 100M
lvreduce -L 100M /dev/vg2/lv2
mount -a
lvdisplay #(Verify)
```
OR
```bash
e2fsck -f /dev/vg1/lvm02
resize2fs -f /dev/vg1/lvm02
mount /dev/vg1/lvm01 /mnt
lvreduce -L 1G -n /dev/vg1/lvm02
lvdisplay (Verify)
```

### QUESTION 13
Create a volume group, and set 16M as a extends. And divided a volume group containing 50 extends on volume group lv, make it as ext4 file system, and mounted automatically under /mnt/data.

```bash
pvcreate /dev/sda7 /dev/sda8
vgcreate -s 16M vg1 /dev/sda7 /dev/sda8
lvcreate -l 50 -n lvm02
mkfs.ext4 /dev/vg1/lvm02
blkid /dev/vg1/lv1
vim /etc/fstab
mkdir -p /mnt/data
vim /etc/fstab
    UUID=xxxxxxxx /mnt/data ext4 defaults 0 0
mount -a
mount
```

### QUESTION 14
The Additional group of the two users: user2, user3 is the admin group Password: redhat

```bash
useradd -G admin user2
useradd -G admin user3
passwd user2
    redhat
passwd user3
    redhat
```

### QUESTION 15
Copy /etc/fstab to /var/tmp name admin, the user1 could read, write and modify it, while user2 without any permission.

```bash
cp /etc/fstab /var/tmp/
chgrp admin /var/tmp/fstab
setfacl -m u:user1:rwx /var/tmp/fstab
setfacl -m u:user2:--- /var/tmp/fstab
ls -l
    -rw-rw-r--+ 1 root admin 685 Nov 10 15:29 /var/tmp/fstab
```

### QUESTION 16
Configure a task: plan to run echo "file" command at 14:23 every day.

```bash
crontab -u natasha -e
    23 14 * * * /bin/echo "file"
```
OR
```bash
su - natasha
crontab -e
    23 14 * * * /bin/echo "file"
```

### QUESTION 17
Configure a default software repository for your system. One YUM has already provided to configure your system on http://server.domain11.example.com/pub/ x86_64/Server, and can be used normally.

```bash
yum-config-manager --add-repo=http://content.example.com/rhel7.0/x86-64/dvd”
vim content.example.com_rhel7.0_x86_64_dvd.repo
    gpgcheck=0
Yumcleanall
Yumrepolist
```
### QUESTION 18

Adjust the size of the Logical Volume.
Adjust the size of the vo Logical Volume, its file system size should be 290M. Make sure that the content of this system is complete.
Note: the partition size is rarely accurate to the same size as required, so in the range 270M to 320M is acceptable.

*Addition*
```bash
df -hT
lvextend -L +100M /dev/vg0/vo
Lvscan
xfs_growfs /home/ #home is the mounted directory of the LVM, this step just need to do in the practice environment, and test EXT4 does not need this step.
resize2fs /dev/vg0/vo/ # use this command to update in examination.
df -hT
```
*Subtraction*
```bash
e2fsck -f/dev/vg0/vo
umount /home
resize2fs /dev/vg0/vo #the final required partition capacity is 100M 
lvreduce -l 100M /dev/vg0/vo
mount /dev/vg0/vo/home
df -hT
```
### QUESTION 19

Configure /var/tmp/fstab Permission.
Copy the file /etc/fstab to /var/tmp/fstab. Configure var/tmp/fstab permissions as the following:
Owner of the file /var/tmp/fstab is Root, belongs to group root
File /var/tmp/fstab cannot be executed by any user
User natasha can read and write /var/tmp/fstab
User harry cannot read and write /var/tmp/fstab
All other users (present and future) can read var/tmp/fstab.

```bash
cp /etc/fstab /var/tmp/
/var/tmp/fstab view the owner setfacl -m u:natasha:rw- /var/tmp/fstab setfacl -m u:haryy:--- /var/tmp/fstab
getfacl /var/tmp/fstab #to view permissions
```
### QUESTION 20

Add a swap partition.
Adding an extra 500M swap partition to your system, this swap partition should mount automatically when the system starts up. Don't remove and modify the
existing swap partitions on your system.

```bash
fdisk -cu /dev/vda/ #in the way of expanding the partition, don’t make main partition
partx –a /dev/vda
mkswap /dev/vdax
swapon /dev/vdax
swapon –s
vi /etc/fstab
    /dev/vdaxswapswapdefaults0 0
mount -a
```

### QUESTION 21

Search files.
Find out files owned by jack, and copy them to directory /root/findresults

```bash
mkdir/root/findfiles
find / -user jack -exec cp -a {} /root/findfiles/ \; ls /root/findresults
```

### QUESTION 22

Search a String
Find out all the columns that contains the string seismic within /usr/share/dict/words, then copy all these columns to /root/lines.tx in original order, there is no blank
line, all columns must be the accurate copy of the original columns.

```bash
grep seismic /usr/share/dict/words > /root/lines.txt
```

### QUESTION 23

Create a backup
Create a backup file named /root/backup.tar.bz2, contains the content of /usr/local, tar must use bzip2 to compress.

```bash
cd /usr/local
tar –jcvf /root/backup.tar.bz2
mkdir /test
tar –jxvf /root/backup.tar.bz2 –C /test/ #Decompression to check the content is the same as the /usr/loca after
#If the questions require to use gzip to compress. change –j to –z.
```
### QUESTION 24

Create a logical volume
Create a new logical volume as required:
Name the logical volume as database, belongs to datastore of the volume group, size is 50 PE.
Expansion size of each volume in volume group datastore is 16MB.
Use ext3 to format this new logical volume, this logical volume should automatically mount to /mnt/database

```bash
fdisk -cu /dev/vda// Create a 1G partition, modified when needed
partx –a /dev/vda
pvcreate /dev/vdax
vgcreate datastore /dev/vdax –s 16M
lvcreate– l 50 –n database datastore
mkfs.ext3 /dev/datastore/database
mkdir /mnt/database
mount /dev/datastore/database /mnt/database/ df –Th
vi /etc/fstab
    /dev/datastore /database /mnt/database/ ext3 defaults 0 0 mount –a
```

### QUESTION 25

Configure your Host Name, IP Address, Gateway and DNS.
Host name: dtop5.dn.ws.com
IP Address: 172.28.10.5/4
Gateway: 172.28.10.1
DNS: 172.28.10.1

```bash
vim /etc/sysconfig/network
    NETWORKING=yes HOSTNAME=dtop5.dn.ws.com GATEWAY=172.28.10.1
vim /etc/hosts
    172.28.10.5 dtop5.da.ws.com dtop5 # Added by NetworkManager 
    127.0.0.1 localhost.localdomain localhost 
    ::1 dtop.da.ws.com dtop5 localhost6.1oeoldomaia6 localhost6
vim /etc/resolv.conf
    # Generated by NetworkManager
    Search dn.ws.com
    Nameserver 172.28.10.1
vim /etc/sysconfig/network-scripts/ifcfg-eth0
    DEVICE="eth0" 
    NM_CONTROLLED="yes" 
    ONBOOT=yes 
    TYPE=Ethernet 
    BOOTPROTO=none 
    IPADDR=172.28.10.5 
    PREFIX=24 
    GATEWAY=172.28.10.1 
    DNS1=172.28.10.1 
    DOMAIN=dn.ws.com 
    DEFROUTE=yes 
    IPY4FAILURE_FATAL=yes 
    IPV6INTr=no 
    NAME="System eth0" 
    TJUM=5tb06b80-Obb0-71M-45f1-86edd65f3e03 
    HWADDR=00,0c:29:0E:A6:C8 
```
### QUESTION 26

Please open the ip_forward, and take effect permanently.
```bash
vim /etc/sysctl.conf net.ipv4.ip_forward = 1
sysctl –w #(takes effect immediately)
# If no “sysctl.conf” option, use these commands:
sysctl –a |grep net.ipv4
sysctl –P net.ipv4.ip_forward = 1
sysctl -w
```
### QUESTION 27

Make on data that only the user owner and group owner member can fully access.
```bash
chmod 770 /data
ls -ld /data # verify, preview should be like:
    drwxrwx--- 2 root sysadmin 4096 Mar 16 18:08 /data
#To change the permission on directory we use the chmod command.
#According to the question that only the owner user (root) and group member (sysadmin) can fully access the directory so: chmod 770 /data
```
### QUESTION 28

Who ever creates the files/directories on a data group owner should automatically be in the same group owner as data.

```bash
chmod g+s /data
ls -ld /data #verify. Permission should be like this: 
    drwxrws--- 2 root sysadmin 4096 Mar 16 18:08 /data
# If SGID bit is set on directory then who every users creates the files on directory group owner automatically the owner of parent directory. To set the SGID bit:
# chmod g+s directory To Remove the SGID bit: chmod g-s directory
```
### QUESTION 29

Your System is going to use as a Router for two networks. One Network is 192.168.0.0/24 and Another Network is 192.168.1.0/24. Both network's IP address has assigned. How will you forward the packets from one network to another network?

```bash
echo "1" >/proc/sys/net/ipv4/ip_forward
vi /etc/sysctl.conf
    net.ipv4.ip_forward = 1
# If you want to use the Linux System as a Router to make communication between different networks, you need enable the IP forwarding. To enable on running session, set value 1 to /proc/sys/net/ipv4/ip_forward. As well as automatically turn on the IP forwarding features on next boot set on /etc/sysctl.conf file.
```
### QUESTION 30

/data Directory is shared from the server1.example.com server. Mount the shared directory that.

```bash
vi /etc/auto.master
/mnt /etc /auto.misc --timeout=50
vi /etc/auto.misc
    data -rw,soft,intr server1.example.com:/data
service autofs restart
chkconfig autofs on
# When you mount the other filesystem, you should unmount the mounted filesystem, Automount feature of linux helps to mount at access time and after certain seconds, when user unaccess the mounted directory, automatically unmount the filesystem.
# /etc/auto.master is the master configuration file for autofs service. When you start the service, it reads the mount point as defined in /etc/auto.master.
```
### QUESTION 31

Create one partitions having size 100MB and mount it on data.
```bash
fdisk /dev/hda #to create new partition.
    n # For New partitions.
    l # for logical parition
    Enter #When asked for the Starting Cylinder, select Default
    +100M # Type the Size. You can specify either Last cylinder of size here.
    P # to verify the partitions lists and remember the partitions name.
    w # to write on partitions table.
partprobe
mkfs -t ext3 /dev/hda
```
OR
```bash
mke2fs -j /dev/hda # To create ext3 filesystem.
vi /etc/fstab
    /dev/hda? /data ext3 defaults 1 2
```
### QUESTION 32

SELinux must run in force mode.
```bash
/etc/sysconfig/selinux
    SELINUX=enforcing
```
### QUESTION 33

The firewall must be open.

```bash
/etc/init.d/iptables start
iptables -F
iptables -X
iptables -Z
/etc/init.d/iptables save
```
### QUESTION 34

In the system, mounted the iso image /root/examine.iso to/mnt/iso directory. And enable automatically mount (permanent mount) after restart system.

```bash
vi /etc/fstab
    /root/examine.iso /mnt/iso iso9660 loop 0 0 mount -a
mount | grep examine
```

### QUESTION 35
Configure your NFS services. Share the directory by the NFS Shared services.
```bash
/etc/init.d/rpcbind start
/etc/init.d/nfslock start
/etc/init.d/nfs start
chkconfig rpcbind on
chkconfig nfslock on
chkconfig nfs on
showmount -e localhost
```
### QUESTION 36

1. Find all sizes of 10k file or directory under the /etc directory, and copy to /tmp/findfiles directory.
2. Find all the files or directories with Lucy as the owner, and copy to /tmp/findfiles directory.

```bash
find /etc -size 10k -exec cp {} /tmp/findfiles \;
find / -user lucy -exec cp -a {} /tmp/findfiles \;
#Note: If find users and permissions, you need to use cp - a options, to keep file permissions and user attributes etc.
```
### QUESTION 37

There is a local logical volumes in your system, named with common and belong to VGSRV volume group, mount to the /common directory. The definition of size is 128 MB.
Requirement:
Extend the logical volume to 190 MB without any loss of data. The size is allowed between 160-160 MB after extending.
```bash
lvextend -L 190M /dev/mapper/vgsrv-common resize2fs /dev/mapper/vgsrv-common
```
### QUESTION 38

Copy /etc/fstab document to /var/TMP directory. According the following requirements to configure the permission of this document.
The owner of this document must be root.
This document belongs to root group.
User mary have read and write permissions for this document.
User alice have read and execute permissions for this document.
Create user named bob, set uid is 1000. Bob have read and write permissions for this document.
All users has read permission for this document in the system.

```bash
cp /etc/fstab /var/tmp
chown root:root /var/tmp/fstab
chmod a-x /var/tmp/fstab
setfacl –m u:mary:rw /var/tmp/fstab
setfacl –m u:alice:rx /var/tmp/fstab
useradd –u 1000 bob
```
### QUESTION 39

There is a local logical volumes in your system, named with shrink and belong to VGSRV volume group, mount to the /shrink directory. The definition of size is 320MB.
Requirement:
Reduce the logical volume to 220 MB without any loss of data. The size is allowed between 200-260 MB after reducing.

```bash
cd; umount /shrink
e2fsck -f /dev/mapper/vgsrv-shrink
resize2fs /dev/mapper/vgsrv-shrink 220M
lvreduce -L 220M /dev/mapper/vgsrv-shrink
mount -a
```
### QUESTION 40

One Domain RHCE is configured in your lab, your domain server is server1.example.com. nisuser2001, nisuser2002, nisuser2003 user are created on your server 192.168.0.254:/rhome/stationx/nisuser2001. Make sure that when NIS user login in your system automatically mount the home directory. Home directory is separately shared on server /rhome/stationx/ where x is your Station number.

```bash
authconfig --nisserver=<NIS SERVER> --nisdomain=<NIS DOMAIN> -- update
# authconfig --niserver=192.168.0.254 --nisdomain=RHCE --update or system-config-authentication
# Click on Enable NIS
# Type the NIS Domain: RHCE
# Type Server 192.168.0.254 then click on next and ok
# You will get a ok message.
# Create a Directory /rhome/stationx where x is your station number.
vi /etc/auto.master
    /rhome/stationx /etc/auto.home --timeout=60
vi /etc/auto.home and write
    * -rw,soft,intr 192.168.0.254:/rhome/stationx/&
# Note: please specify your station number in the place of x.
service autofs restart
#Login as the nisuser2001 or nisuser2002 on another terminal will be Success. According to question, RHCE domain is already configured. We have to make a client of RHCE domain and automatically mount the home directory on your system. To make a member of domain, we use the authconfig with option or system-config authentication command. There a are lots of authentication server i.e NIS, LDAB, SMB etc. NIS is a RPC related Services, no need to configure the DNS, weshould specify the NIS server address. Here Automount feature is available. When user tried to login, home directory will automatically mount. The automount service used the /etc/auto.master file. On /etc/auto.master file we specified the mount point the configuration file for mount point.
```
### QUESTION 41

Upgrade the kernel, start the new kernel by default. kernel download from this address:
ftp://server1.domain10.example.com/pub/update/new.kernel
```bash 
ls
kernel-2.6.32-71.7.1.el6.x86_64.rpm
kernel-firmware-2.6.32-71.7.1.el6.noarch.rpm

rpm -ivh kernel-*
# Verify the grub.conf file, whether use the new kernel as the default boot.
cat /boot/grub/grub.conf 
    default=0 title Red Hat Enterprise Linux Server (2.6.32-71.7.1.el6.x86_64) root (hd0,0)
    kernel /vmlinuz-2.6.32-71.7.1.el6.x86_64 ro root=/dev/mapper/vol0-root rd_LVM_LV=vol0/root rd_NO_LUKS rd_NO_MD
    rd_NO_DM LANG=en_US.UTF-8 SYSFONT=latarcyrheb-sun16 KEYBOARDTYPE=pc KEYTABLE=us crashkernel=auto rhgb quiet
    initrd /initramfs-2.6.32-71.7.1.el6.x86_64.img
```
### QUESTION 41

Open kmcrl value of 5 , and can verify in /proc/ cmdline

```bash
vim /boot/grub/grub.conf
    kernel/vmlinuz-2.6.32-71.el6.x86_64 ro root=/dev/mapper/GLSvg-GLSrootrd_LVM_LV=GLSvg/GLSroot
    rd_LVM_LV=GLSvg/GLSswaprd_NO_LUKSrd_NO_MDrd_NO_DM
    LANG=en_US.UTF-8 SYSFONT=latarcyrheb-sun16 KEYBOARDTYPE=pc KEYTABLE=us crashkernel=auto rhgb quiet kmcrl=5
# Restart to take effect and verification:
cat /proc/cmdline
    ro root=/dev/mapper/GLSvg-GLSroot rd_LVM_LV=GLSvg/GLSroot rd_LVM_LV=GLSvg/GLSswap rd_NO_LUKS rd_NO_MD rd_NO_DM
    LANG=en_US.UTF-8 SYSFONT=latarcyrheb-sun16 KEYBOARDTYPE=pc KEYTABLE=us rhgb quiet kmcrl=5
```

### QUESTION 42

Configure autofs.
Configure the autofs automatically mount to the home directory of LDAP, as required: server.domain11.example.com use NFS to share the home to your system. This file system contains a pre configured home directory of user ldapuserX. Home directory of ldapuserX is: server.domain11.example.com /home/guests/ldapuser

Home directory of ldapuserX should automatically mount to the ldapuserX of the local /home/guests Home directory’s write permissions must be available for users
ldapuser1’s password is password

```bash
yum install -y autofs
mkdir /home/rehome
/etc/auto.master
/home/rehome/etc/auto.ldap
Keep then exit
cp /etc/auto.misc /etc/auto.ldap
https://www.gratisexam.com//etc/auto.ldap
ldapuserX -fstype=nfs,rw server.domain11.example.com:/home/guests/
# Keep then exit
systemctl start autofs
systemctl enable autofs
su - ldapuserX// test
# If the above solutions cannot create files or the command prompt is -bash-4.2$, it maybe exist multi-level directory, this needs to change the server.domain11.example.com:/home/guests/ to server.domain11.example.com:/home/guests/ldapuserX. What is multi-level directory? It means there is a directory of ldapuserX under the /home/guests/ldapuserX in the questions. This directory is the real directory.
```