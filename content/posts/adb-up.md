---
title : 'ADB Up'
slug : 'adb-up'
tags :
    - Android
description : 'Exploring the world of ADB for beginners'
date : 2022-02-01
toc : true
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---

Android Debug Bridge (adb) is a versatile command-line tool that lets you communicate with a device. The adb command facilitates a variety of device actions, such as installing and debugging apps, and it provides access to a Unix shell that you can use to run a variety of commands on a device. It is a client-server program that includes three components:
- A client, which sends commands. The client runs on your development machine. You can invoke a client from a command-line terminal by issuing an adb command.
- A daemon (adbd), which runs commands on a device. The daemon runs as a background process on each device.
- A server, which manages communication between the client and the daemon. The server runs as a background process on your development machine.

If you still didn’t get the gist of it, allow me to break it down even further. ABD is two different applications: one is running on your computer, which could be running Windows 10, Linux or even macOS, while the other application is running on your phone. When your phone connects to the machine that is running any desktop operating system, and if USB debugging is enabled, you can issue commands and communicate with the phone using the command line utility.

The Android OS itself is based on Linux and therefore, it uses the Linux kernel and tools as a base. What this means is that there are quite a few Linux commands that can be sent through the ADB server (running on your computer) to the ADB client (running on your phone). Using the server-client relationship, this tag team can become a beneficial tool as you will be able to debug things when nothing or very little is going right for you. Also, you can execute unexplored commands when you’re on your hacking spree, so as far as exploration goes, there are several options present at your fingertips.

---

We will try to understand few ADB commands so that you can use our smartphone to its fullest potential.

## Prerequisites

Before you do anything remotely doohickey, you have to enable `Developer Options` on your smartphone. This is generally acessed by tapping `Build Number` 7 times. You can find this option in one of the following locations, depending on your Android version:
- Android 9 (API level 28) and higher: Settings > About Phone > Build Number
- Android 8.0.0 (API level 26) and Android 8.1.0 (API level 26): Settings > System > About Phone > Build Number
- Android 7.1 (API level 25) and lower: Settings > About Phone > Build Number

Before you can use the debugger and other tools, you need to enable `USB debugging`, which allows Android Studio and other SDK tools to recognize your device when connected via USB. To enable USB debugging, toggle the USB debugging option in the Developer Options menu. You can find this option in one of the following locations, depending on your Android version:
- Android 9 (API level 28) and higher: Settings > System > Advanced > Developer Options > USB debugging
- Android 8.0.0 (API level 26) and Android 8.1.0 (API level 26): Settings > System > Developer Options > USB debugging
- Android 7.1 (API level 25) and lower: Settings > Developer Options > USB debugging

## Start ADB Server 

Before executing some cool commands, you need to start the ADB Server. Once you start the ADB Server, you can interact with your Android device. However, make sure to connect your Android device to the computer before starting the ADB Server. You can enter the following command to Start ADB Server.

```shell
adb start-server
```

## List all Connected Devices 

After connecting Android devices, users should know whether the ADB bridge is working or not. To confirm that, users need to check whether the connected devices are showing on the ADB or not. So, users just need to enter the following command to list connected Android devices.

```shell
adb devices
```

## Know the Current State of the device 

This command is useful when the touch response of your Android device is not working. Through this command, you will be able to know whether your device is in offline, bootloader or in device mode. For a normal state, you will see Android state as `Device'. Here's the adb command that you can use

```shell
adb get-state
```

## Get Android's Serial Number 

Android has few apps which tell the serial number. However, most of the apps need root access to work. Since not everyone has a rooted device, using an adb command to know the serial number sounds nice. This command will let you know the device serial number of the connected device.

```shell
adb get-serialno
```

## Create Full Backup 

Android provides users with lots of option to create a full backup of your smartphone. However, these backups are usually stored on phone storage. So, if you choose to reset your device for any reason, all your backups will be removed. However, with the below android shell commands, you can create a backup of your smartphone directly on your computer.

```shell
adb backup -all -f /backup/location/file.ab
```

## Restore Backup 

After creating a backup, its time to know how to restore. Restoring the backup to the phone via ADB commands is super easy, and you just need to enter the command given below. Make sure to change the 'backup-file-location' with the location where you backup has been saved.

```shell
adb restore backup-file-location
```

## Installing Multiple Apps 

If you are trying to sideload one or two apps, then manual installation is the best way. However, if you have more than 20 apps to install, then you need to take the help of ADB. With the help of ADB, you can easily install multiple Apk files on your Android smartphone. You can use the adb devices command given below to install multiple apk files at once on Android.

```shell
for %f in (folder-path*.apk) do adb install %f
```

Make sure to change the 'Folder-path' with your file destination.

## List Apps

You can print the list of the app package names for all apps installed on your Android device. You can use this command with different parameters to get a more specific list of app packages.

For instance, if you want to list the system apps only, use

```shell
adb shell pm list packages -s
```

In order to list all third-party apps installed on your Android phone or tablet, you issue the following command.
```shell
adb shell pm list packages -3
```
If you want ADB Shell to show the list of all enabled or disabled apps on your device, try the command with parameters like `-d` (for disabled apps), `-e` (for enabled apps), and `-u` (for uninstalled apps).
```shell
adb shell pm list packages -d
```
```shell
adb shell pm list packages -e
```
```shell
adb shell pm list packages -u
```
To list app packages with specific keywords filters.
```shell
adb shell pm list packages <keywords>
```
To find the list of apps along with their associated packages, execute the following command
```shell
adb shell pm list packages -f
```
You can easily get a list of group packages by a certain manufacturer, or come common term. For instance, if you want to list all apps by Google, you can use the following command.
```shell
adb shell pm list packages | grep 'google'
```
You can replace “google” with “samsung”, “huawei”, “xiaomi”, “miui”, “evenwell”, “android”, “facebook”, etc. to get desired list of packages.

## Uninstall System App 

Well, there some bloatwares that doesn't uninstall unless you root your device. However, you can uninstall those system apps through adb commands. So, to uninstall an app, you need to execute the following command. However, make sure to replace the `package-name` with the actual package you got from previous step.
```shell
adb shell pm uninstall --user 0 "package-name"
```
## Reinstall Uninstalled System App

If you want to reinstall system app which you have unknowingly uninstalled in previous step. You can reinstall same apps again, provided you have exact name of the package.

```shell
adb shell cmd package install-existing "package-name"
```

## Disable System App
The problem with uninstalling system app is twofold: 
- it doesn’t actually fully uninstall the app and return space to the user and 
- reverting the change requires you to either sideload the APK (if you can find it) or factory reset. 

Still, previous method is quite useful and dozens of forum posts and user scripts taking advantage of it to debloat new Android devices. To help you debloat your device in a safer way, we will look at another method that will not only disable the pre-installed bloatware of your choosing but also make it super easy to re-enable them at your convenience, making any mistake a lot easier to recover from. However, make sure to replace the `package-name` with the actual package you got from list packages step.
```shell
adb shell pm disable-user --user 0 "package-name"
```
## Re-enable Disabled System App

What if you disabled an app and want it back? It’s very easy to re-enable the app! First, go to Settings > Apps and look at the “All apps” list (it may be located somewhere different on your device.) Usually, you can filter here to see the names of all disabled apps. Once you know what app you want to re-enable, follow these steps:

1. This command lists all disabled packages. Find the package name that corresponds to the app you want to re-enable.

```shell
adb shell pm list packages -d
``` 
2. Now, just run the following command to re-enable one of them:
```shell
adb shell pm enable "package-name"
```
## Hide Apps

This command will hide the app from the launcher.

```shell
adb shell pm hide --user 0 "package-name"
```

## Record Screen 
You will find tons of screen recorder apps on the Google Play Store. These screen recording tools often come with few bugs, and it annoys a lot by showing ads. Moreover, third-party screen recorders put watermarks on your recorded videos. However, all hassles end with ADB. You can use ADB to record your Android screen. You just need to enter the following command given below to record screen without using an app. Besides, you can also set conditions like video duration, resolution in pixels and video bitrate, etc.

```shell
adb shell screenrecord folder-path/ filename.mp4
```
You can stop screen recording using Ctrl+C. In case you want to record the screen in a specific resolution, the following command lets you set custom width and height in pixels.
```shell
adb shell screenrecord --size 1920x1080 folder-path/ filename.mp4
```
By default, Android’s screen recorder’s duration is set to 180 seconds (3 minutes). You can decrease this time limit according to your needs (180 seconds is the maximum limit).
```shell
adb shell screenrecord --time-limit 120 folder-path/ filename.mp4
```
Similarly, you can also determine the bitrate of the video output. To set the bitrate to 4MBPS, for example, you can use the following value:
```shell
adb shell screenrecord --bit-rate 6000000 folder-path/ filename.mp4
```
Make sure to change the 'Folder-path' and `filename' before executing the command.

## Get System Information 
Developers use a shell command known as dumpsys to grab system information. You can also use the same command to get detailed information about your smartphone's system. The command also throws lights on the hardware information as well. You need to enter the following command to get system stats and info. The command will list out all commands that you can use with dumpsys to grab information. You can later use those command to get the most accurate hardware or software information.
```shell
adb shell dumpsys I grep DUMP OF SERVICE
```

## Take Screenshots 
Just like Android screen recording, you can also use ADB commands to take a quick screenshot. If your smartphone doesn't come with a built-in screenshot taker, then you don't need to install any third party app. You need to use the following command to take a screenshot on your Android.
```shell
adb shell screencap -p /path/to/ screenshot.png
```
Make sure to replace the "/path/to/ screenshot" with your destination path before executing the command.

## Reboot to Recovery Mode 
If your Android fails to boot, you need to enter the Recovery mode. Normally, Android users need to use some key combination to reach the Recovery mode. However, there's an ADB command available which can force your Android to reboot into Recovery mode. You can use the code given below to Reboot Android into Recovery Mode.
```shell
adb reboot-recovery
```
## Reboot to Bootloader 
There's an ADB Command which forces the connected device to boot into the bootloader. Bootloader mode is pretty much similar to the fast boot mode. Here's the command to boot your Android into Bootloader.
```shell
adb reboot-bootloader
```
## Display the log data on the screen 
If you are searching for an ADB command to display the log data onto the screen, then you need to try the ADB Command given below. this command will display all log data right on the screen.
```shell
adb logcat
```
## Wait before executing a command 
If you are searching for an ADB command that can be used to program delay before the next command, then you need to use the given command. This command will automatically execute the next command when the device is ready.
```shell
adb wait-for-device
```
## Pull Files from Android to Computer 
This is much like a copy paste command for ADB. The command basically pulls any files that are saved on your device and stores it on your computer. Here's the ADB command to pull any files from Android to PC.
```shell
adb pull [mention the file path on device] [mention the destination file path on your Windows]
```
## Copy Files from Computer To Android 
Just like the pull command, push command can be used to transfer files from computer to your smartphone. So, here the command to push a file from your computer to your device.
```shell
adb push [mention the file path on computer] [mention the destination file path on your Android]
```
## ADB Shell KeyEvent commands

Android devices support KeyEvent commands that can let you perform certain actions that require you to press a hardware button or tap an app or UI option. You can control your Android phone or tablet device simply by using these KeyEvent commands. These commands might come in handy if the hardware keys on your device are not functioning properly due to some damage.

- Turn Android device ON or OFF: 
```shell 
adb shell input keyevent 2 
```
- Press Home button: 
```shell 
adb shell input keyevent 3 
```
- Press Back button: 
```shell 
adb shell input keyevent 4 
```
- Press Call button: 
```shell 
adb shell input keyevent 5 
```
- End a call: 
```shell 
adb shell input keyevent 6 
```
- Press Power Button to wake up screen: 
```shell 
adb shell input keyevent 26 
```
- Turn ON the camera: 
```shell 
adb shell input keyevent 27 
```
- Open wen browser: 
```shell 
adb shell input keyevent 64 
```
- Press the Enter key: 
```shell 
adb shell input keyevent 66 
```
- Press Backspace button: 
```shell 
adb shell input keyevent 67 
```
- Open Contacts app: 
```shell 
adb shell input keyevent 207 
```
- Decrease display brightness: 
```shell 
adb shell input keyevent 220 
```
- Increase Display brightness: 
```shell 
adb shell input keyevent 221 
```
- Cut text: 
```shell 
adb shell input keyevent 277 
```
- Copy text: 
```shell 
adb shell input keyevent 278 
```
- Paste text: 
```shell 
adb shell input keyevent 279 
```
- Make the device sleep: 
```shell 
adb shell input keyevent KEYCODE_SLEEP 
```
- Make device wakeup: 
```shell 
adb shell input keyevent KEYCODE_WAKEUP 
```
- Toggle Power menu: 
```shell 
adb shell input keyevent KEYCODE_POWER 
```
## Stop ADB Server 
This is the final command which you need to enter after doing all your works. You need to enter the following command to stop the ADB Server that you have started in the very first step. You can enter the following command to Stop ADB Server.
```shell
adb kill-server
```