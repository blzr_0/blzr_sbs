---
title : '18 Money Rules'
slug : '18-money-rules'
tags :
    - Personal
    - Thoughts
    - Random Access Memories
    - Resource
description : '18 money rules everyone should learn by 25'
date : 2024-03-20
toc : false
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---
1. Pay yourself first. As soon as you get paid, put money into savings. Automating this is even better.
2. Keep a 6-month emergency fund.
> If you have multiple streams of income, you can go as low as 3 months. If starting out on your own, you could need as much as 12 months.
3. Budget using the `50/30/20` rule.
    - 50% for needs
    - 30% for wants
    - 20% towards saving /investing
> This is the bare minimum!
4. Divide your bonus into thirds:
    - 1/3 for fun
    - 1/3 for retirement
    - 1/3 for debt paydown (add to retirement if only low-interest debt)
5. Put all, or a large percentage, of your raises into saving and investing. This helps avoid lifestyle inflation and moves up your retirement date.
6. Avoid high-interest debt. If you have it, use the avalanche or snowball method to pay it off.
7. Your home payment (mortgage, interest, insurance) should cost less than 25% of your monthly income.
8. When buying a car use the `20/4/10` Rule if you have to.
    - 20% down
    - 4-year loan
    - < 10% of your monthly income
9. You should save at least 15% of your income for retirement.
10. Your age subtracted from 100 represents the percentage of stocks you should have in your portfolio.
Some are now using the number 120.
11. The stock market has a longterm average return of 10%. To calculate your returns, it’s common to use 6-8% to capture the effect of inflation.
12. The rule of 72 to tells you how long it will take your investment to double.

Example: The stock market returns 10%, so 72/10 = 7.2 years to double your money.

13. The 4-percent rule says you can safely withdraw 4% of your starting investment balance each year (adjust for inflation in subsequent years) and not run out of money.

14. Your Net Worth should be equal to your `age x Pre-Tax Income / 10`.

Example: if you are 35 years old and $100,000 in annual income, then your net worth should be $350,000 (35 x 100000 / 10).

15. Have at least five times your gross salary in term life insurance.

16. Before spending money, wait 24 hours and ask: do I still want it? If you do, go ahead and buy it. This will save you from a lot of impulse purchases.

17. Save for retirement first, then your children’s education.

18. Value time over money and experience over things.