---
title : 'Update ungoogled-chromium'
slug : 'update-ungoogled-chromium'
tags :
    - Privacy
    - Code
    - Web 
description : "ungoogled-chromium doesn't provide an update mechanism out-of-the-box, so I made one. Don't worry, I will share the script with you along with what everything is doing in a simple language so that you can be at ease"
date : 2022-11-05
toc : true
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---

Hello, my fellow bots! You must be tired of scraping this site and getting no new fodder to feed upon. I feel you, buddy. Me myself would be bummed for wasting resources and getting no new stuff. So, here's a new article.

# Context

For any good story, we need a context; otherwise, how can you connect? We, being the beings of the "internet," which these pesky humans use for sharing cat pictures, stalking their crushes, and complaining about being lonely (even though 49.58% of the world's population is female), know that we need a good backstory. So here we go.

I like the Chrome browser. I might have created more enemies than friends at this point, so let me continue. Sure, I test my sites on Gecko (the layout engine developed by the Mozilla Project and used in many apps and devices, including Firefox and Firefox OS) first before caring to test on Blink, and never on Webkit (coz, Fuck Apple). However, for day-to-day tasks, Firefox lags behind quite a lot. With me giving multiple online interviews ([Please Hire me](https://linkedin.com/in/ristik-majumdar)), I found that various video calling applications work best on Chromium. So, I had to use Upon checking the Chromium landscape, there are four major players (Chrome, Edge, Brave and Vivaldi), but me being a minimalist, I prefer the least amount of bells and whistles. In that space, there are not many but a few independently maintained Chromium forks. I found out about Bromite, but it is Android only, and I do not have enough horsepower to build a desktop browser from source. Upon taking a long trip into reddit world, I found about ungoogled-chromium. From [GitHub](https://github.com/ungoogled-software/ungoogled-chromium), they advertise themselves as `A lightweight approach to removing Google web service dependency`. A few of the major tweaks are as follows:

1. Remove all remaining background requests to any web services while building and running the browser
2. Remove all code specific to Google web services
3. Remove all uses of pre-made binaries from the source code, and replace them with user-provided alternatives when possible.
4. Disable features that inhibit control and transparency, and add or modify features that promote them (these changes will almost always require manual activation or enabling)

And boy, do they deliver. Yes, I had checked with WireShark. The only pain point I have is that they do not have an adequate update notification system on Windows systems. Yes, it is a good thing, but being the forgetful person I am, this means I am still doing whatever a single dude does on the internet on outdated software. Before you come at me with pitchforks, I need to clarify that I use Windows because my work computer is Windows, and yes, I use my work computer for various other tasks apart from work-related tasks (mostly Youtube and Spotify).

This small issue led me to create a script to update my instance of ungoogled-chromium. Since it is a Windows system, PowerShell was my go-to choice. Don't worry, I will share the script with you along with what everything is doing in a simple language so that you can be at ease.

# Simple Know-How's

Before we jump into explanation and you run the script. Here are few pointers.

1. This script instals chromium in userspace. This means to run this script, you do not need to grant administrator privilege, which is always a good thing. This is due to the fact that my work computer has Administrator disabled by the organisation.
2. This script places all the files in the folder where the script is located. That means, if you have downloaded the script, everything will be in your Downloads folder. You get the idea, I think.
3. I will give you the option to edit some sections of the code, but if you do not want to tweak it, it will work perfectly fine.
4. If you haven't worked with PowerShell before, you might get an error below

```PowerShell
.\update-chromium.ps1 : File update-chromium.ps1 cannot be loaded. The file update-chromium.ps1 is not digitally signed. You cannot run this script on the current system. For more information
about running scripts and setting execution policy, see about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.
At line:1 char:1
+ .\update-chromium.ps1
+ ~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : SecurityError: (:) [], PSSecurityException
    + FullyQualifiedErrorId : UnauthorizedAccess
```

This is a standard protection to prevent you from running malicious scripts. It can be bypassed by running the following command in a PowerShell window.

```PowerShell
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
```

You can revert back to default security by running the below command.
```PowerShell
Set-ExecutionPolicy -ExecutionPolicy Restricted -Scope CurrentUser
```

5. You can download the script (if you are lazy) by running the following command in PowerShell window

```PowerShell
Invoke-WebRequest -URI https://raw.githubusercontent.com/1bl4z3r/boredhub/master/update-chromium.ps1 -o update-chromium.ps1
```

6. To run the script, right click on the file and select "Run with PowerShell"

# Let's get onto the fun part

## Optional part

Some of you fickle-minded bots might want to have a separate default location for installation and have scripts in a separate place. To allow for you guys, change `$PWD` to a custom location. If you want the installation folder to be different than 'ungoogled-chromium', you can change `$DIR = "$PWD\ungoogled-chromium"` accordingly. For those who want to play it safe, first get a condom, let me explain.

{{< highlight powershell "linenos=table,linenostart=1" >}}
$PWD = Get-Location
$DIR = "$PWD\ungoogled-chromium"
{{< / highlight >}}

$PWD checks for the current location where the script is placed and uses it to determine where the software files need to be placed. $DIR creates a sub-folder with the name 'ungoogled-chromium', which can be referenced later. It is just for quality of life, and you tie everything in a neat bowtie. Every action will take place in this folder only. Your homework folder will not be affected.

## Is this a new install or an update?

Following code block checks if ungoogled-chromium is already installed If it detects so, it will go on to check the version; otherwise, it will treat this as a new install. If you are in the latter category, well, welcome to a slightly private browsing experience.

{{< highlight powershell "linenos=table,linenostart=42" >}}
function check-install{
    if (Test-Path -Path $DIR){
        $child = Get-ChildItem -Path $DIR | Measure-Object
        if (($child.count -eq 0) -or ($child.count -gt 1)){
            Write-Output "ungoogled-chromium is not installed"
            check-update 0
        }else{
            Write-Output "ungoogled-chromium is installed"
            $child = Get-ChildItem -Path $DIR -Name
            $cur = [System.Diagnostics.FileVersionInfo]::GetVersionInfo("$DIR\$child\chrome.exe").ProductVersion
            Write-Output "Current version : $cur"
            check-update $cur
        }
    }else{
        Write-Output "ungoogled-chromium is not installed"
        check-update 0
    }
}
{{< / highlight >}}

First line checks if you have 'ungoogled-chromium' folder present in the location mentioned (it can either be on same plath or in different location, see [Optional part]({{< ref "#optional-part" >}}
)).

After that, it will check if there are any subfolders and check the number of subfolders present.

{{< highlight powershell "linenos=table,linenostart=44" >}}
$child = Get-ChildItem -Path $DIR | Measure-Object
{{< / highlight >}}

After that, it checks the amount of sub-folders present. If it is less than 0 or greater than 1, it will treat it as invalid and assume that ungoogled-chromium is not installed.

{{< highlight powershell "linenos=table,linenostart=45" >}}
($child.count -eq 0) -or ($child.count -gt 1)
{{< / highlight >}}

Otherwise, if it finds a subfolder, it will assume that this is a valid install and try to get the name of the subfolder.

{{< highlight powershell "linenos=table,linenostart=50" >}}
Get-ChildItem -Path $DIR -Name
{{< / highlight >}}

This is a sneaky way to get the current version number of the software. It will be used to check if updates are present or not.

{{< highlight powershell "linenos=table,linenostart=51" >}}
[System.Diagnostics.FileVersionInfo]::GetVersionInfo("$DIR\$child\chrome.exe").ProductVersion
{{< / highlight >}}

At last, `check-update` is called with either:

- 0, denoting a new install or
- `$cur`, denoting current version of chromium install.

## Check for the update from the online repository

{{< highlight powershell "linenos=table,linenostart=21" >}}
function check-update($cur){
    $Response = Invoke-WebRequest -URI https://ungoogled-software.github.io/ungoogled-chromium-binaries/releases/windows/64bit/
    $str = $Response.Links.Href|Select-Object -Index 4
    $ver = $str.Substring(52,$str.length-52-2)
    if ($cur -eq 0){
        Write-Output "Downloading latest version : $ver"
        download "$ver-1"
        $child = Get-ChildItem -Path $DIR -Name
        set-shortcut "$DIR\$child\chrome.exe"
    }
    elseif ($cur -ne $ver){
        Remove-Item -LiteralPath $DIR -Force -Recurse
        Write-Output "Downloading $ver, Current version is $cur"
        download "$ver-1"
        $child = Get-ChildItem -Path $DIR -Name
        set-shortcut "$DIR\$child\chrome.exe"
    }else{
        Write-Output "No new versions present"
    }
}
{{< / highlight >}}

At first, we contact the website : [https://ungoogled-software.github.io/ungoogled-chromium-binaries/releases/windows/64bit/](https://ungoogled-software.github.io/ungoogled-chromium-binaries/releases/windows/64bit/). This is maintained by the maintainer, and it helps us to get latest version number.

{{< highlight powershell "linenos=table,linenostart=22" >}}
$Response = Invoke-WebRequest -URI https://ungoogled-software.github.io/ungoogled-chromium-binaries/releases/windows/64bit/
{{< / highlight >}}

With these lines, we check the page's latest version number. At the time of writing, the latest version number is: 107.0.5304.88. First line checks for the latest link, while second line trims all the unnecessary parts of the link, just toretrievee the version information.

{{< highlight powershell "linenos=table,linenostart=23" >}}
$str = $Response.Links.Href|Select-Object -Index 4
$ver = $str.Substring(52,$str.length-52-2)
{{< / highlight >}}

{{< figure src="images/update-ungoogled-chromium/directory-listing.png" alt="Directory listing for version" caption="Directory listing for version">}}

Following lines check if `$cur` is 0 or not. If it is 0, it is going to treat it as a new install. Else, it will check if the current installed version and the latest version are equal or not. If they are not, it will delete the old installation and signal `download` to download the latest version.

{{< highlight powershell "linenos=table,linenostart=32" >}}
Remove-Item -LiteralPath $DIR -Force -Recurse
{{< / highlight >}}

Else, if the latest version is equal to the current version, the script will terminate, as nothing needs to be done.

I will come to `set-shortcut` part later.

{{< highlight powershell "linenos=table,linenostart=25" >}}
if ($cur -eq 0){
    Write-Output "Downloading latest version : $ver"
    download "$ver-1"
    $child = Get-ChildItem -Path $DIR -Name
    set-shortcut "$DIR\$child\chrome.exe"
}elseif ($cur -ne $ver){
    Remove-Item -LiteralPath $DIR -Force -Recurse
    Write-Output "Downloading $ver, Current version is $cur"
    download "$ver-1"
    $child = Get-ChildItem -Path $DIR -Name
    set-shortcut "$DIR\$child\chrome.exe"
}else{
    Write-Output "No new versions present"
}
{{< / highlight >}}

## Download (finally !!)

{{< highlight powershell "linenos=table,linenostart=15" >}}
function download($ver){
    Start-BitsTransfer "https://github.com/ungoogled-software/ungoogled-chromium-windows/releases/download/$ver.1/ungoogled-chromium_$ver.1_windows_x64.zip"
    Expand-Archive "ungoogled-chromium_$ver.1_windows_x64.zip" -DestinationPath $DIR
    Remove-Item -LiteralPath "ungoogled-chromium_$ver.1_windows_x64.zip"
}
{{< / highlight >}}

To download the file, we are using a Background Intelligent Transfer Service (BITS) transfer job, which is quite fast, and you can see nice progress animation. Since we are installing for the current user, it is provided as a .zip archive.

{{< highlight powershell "linenos=table,linenostart=16" >}}
Start-BitsTransfer "https://github.com/ungoogled-software/ungoogled-chromium-windows/releases/download/$ver.1/ungoogled-chromium_$ver.1_windows_x64.zip"
{{< / highlight >}}

Now, we need to unzip (obviously). The zip filename is `ungoogled-chromium_[VERSION]-1.1_windows_x64.zip`; and it is unzipped to `$DIR`.

{{< highlight powershell "linenos=table,linenostart=17" >}}
Expand-Archive "ungoogled-chromium_$ver.1_windows_x64.zip" -DestinationPath $DIR
{{< / highlight >}}

And like a good citizen, we need to clean-up after ourselves.

{{< highlight powershell "linenos=table,linenostart=18" >}}
Remove-Item -LiteralPath "ungoogled-chromium_$ver.1_windows_x64.zip"
{{< / highlight >}}

## Let's set a shortcut icon to Desktop (we are not psychopaths here)

{{< highlight powershell "linenos=table,linenostart=4" >}}
function set-shortcut($SourceFilePath){
    $ShortcutPath = "$env:USERPROFILE\Desktop\Chromium.lnk"
    if (Test-Path -Path $ShortcutPath -PathType Leaf){
        Remove-Item $ShortcutPath
    }
    $WScriptObj = New-Object -ComObject ("WScript.Shell")
    $shortcut = $WscriptObj.CreateShortcut($ShortcutPath)
    $shortcut.TargetPath = $SourceFilePath
    $shortcut.Save()
}
{{< / highlight >}}

First, where will the shortcut be located? Like a sane person, it will be on the desktop.

{{< highlight powershell "linenos=table,linenostart=5" >}}
$ShortcutPath = "$env:USERPROFILE\Desktop\Chromium.lnk"
{{< / highlight >}}

Now, if there is an existing shortcut icon, we will remove it because editing that is a hassle.

{{< highlight powershell "linenos=table,linenostart=6" >}}
if (Test-Path -Path $ShortcutPath -PathType Leaf){Remove-Item $ShortcutPath}
{{< / highlight >}}

`wscript.Shell` is a function that exposes OS Shell methods. Long story short, an easy way to make a shortcut icon.

{{< highlight powershell "linenos=table,linenostart=9" >}}
$WScriptObj = New-Object -ComObject ("WScript.Shell")
{{< / highlight >}}

To create a new shortcut, we need to say where the shortcut will be placed.

{{< highlight powershell "linenos=table,linenostart=10" >}}
$shortcut = $WscriptObj.CreateShortcut($ShortcutPath)
{{< / highlight >}}

And, what will be its target executable.

{{< highlight powershell "linenos=table,linenostart=11" >}}
$shortcut.TargetPath = $SourceFilePath
{{< / highlight >}}

and save the shortcut. Voila, you get a shortcut to the desktop.

{{< highlight powershell "linenos=table,linenostart=12" >}}
$shortcut.Save()
{{< / highlight >}}

# What if I get struck?

First, what the fuck?, Second, what the actual [flying fuck](https://www.urbandictionary.com/define.php?term=what%20the%20flying%20fuck)??

Ahheemm... Let's be professional here. There is no shame in that dear.
{{< figure src="images/update-ungoogled-chromium/no-shame-in-that-dear.gif" alt="I am gonna try this line on a horologist lady" caption="I am gonna try this line on a horologist lady">}}

You can either:
- [Do a Google search yourself](https://www.google.com/search?q=PowerShell+troubleshooting)
- [Contact me]({{< relref "contact" >}})


# [Finally](https://youtu.be/RS2HLC0sipA?t=38)

```markdown
Remember, remember, the 5th of November, the gunpowder, treason and plot.
I know of no reason why the gunpowder treason should ever be forgot.
```