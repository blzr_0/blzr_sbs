---
title : 'Buying my own domain'
slug : 'buying-my-own-domain'
tags :
    - Personal
    - Web
description : 'What happens when you think with your little head'
date : 2022-07-01
toc : true
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---

This is just a small update for me and 1 ½ people out there reading this shit (½ refers to the automated web scraping bots [`Yes, I See the network logs`], if it wasn't clear).

On 29th of June, I had amassed enough money to buy a cheapest domain possible on Namecheap, which ain't complete pile of garbage. it is [1bl4z3r.cyou](1bl4z3r.cyou). It had cost me a sum total of $48.42 for 10 years, and given that I was running home on a free domain, it had a lot (I mean a lot) of advantages, which I will Copy-Paste here. {emoji}

### A Little backstory

I had started my self controlled, GitHub hosted, lots of Copy-Pasted codes snippets and hair pulls on Nov 28, 2021. Did I remember that? Of course. Its not that I looked into my [commit history](https://github.com/1bl4z3r/1bl4z3r.github.io/commit/35bcf5c3c6f1db54279ce88844e3666a6f4c5cb2) to get the date. Nope. Me, being me and me is a smart-ass, I had an assumption that let's run on free domains, who's gonna look into this mess?

Oh boy, was I wrong!!! No, I was right about 2nd part, where no one, I mean absolutely no one even opens this site by mistake; I was wrong about the other part. Also, I had a bad habit of self-hosting various [shit](https://1bl4z3r.cyou/posts/bitwarden-on-google-cloud) which was not even required to be self-hosted, and creating self-hosted monitoring services to monitor said shit.

Here's a thing about self-hosted services and your services are on public internet. It needs to have a SSL, Now, you can create a self-signed certificate which points to a public IP address, but me is a lazy-ass, and why should I do a thing when there is Let's Encrypt. Do I sound like a hypocrite? Sure. Am I ashamed for this? Pffft... Who do you think I am, a Loser?

Also [Let's Encrypt doesn't provide SSL certificates for Public IP address](https://community.letsencrypt.org/t/can-i-use-my-public-ip-instead-of-a-doman-name/52050)

Where was I? Oh yes. Now I was rocking with all these free domains. A complete set of 69 domains and subdomains. I was high as fuck, getting a orgasm every time I see a free domain loading with `https://`.  This excitement of me being single was short lived as after few months, the load times started to falter. Sometimes the domains would fail to load, sometimes the root servers goes to sleep and sometimes I mistype the domain (which was most of the times), because it is so fucking long and I also had to circumvent my Organization's shitty firewall, so that I can watch totally legally downloaded anime in peace while on the clock. Yep, its `"legally downloaded"`, `"anime"` and  `"on the clock"` in one sentence.

### Getting free knowledge out

Having used a plethora of free domains for 6+ months, I believe I am a perfectly eligible candidate to shove the pros and cons of free domains down your throat till you beg for some air. Let's get you undressed.

#### Pros of Free domains, because you are smart

1. Well, it's free

The main advantage of free domain registration is the cost—or lack thereof. For the most part, these free domains are good for one year, which also means you may have to pay after the initial year. Sometimes, for Freenom domains, you need to renew after 1 year. Since it is free, there are no fees to be paid every year. This is certainly very helpful for beginner bloggers (like me) who still want to learn to tinker with websites for learning purposes. It is also useful.

2. Easy to get started

Since it is free, there is little to no friction on getting started. You can immediately launch an application within a couple of hours. Provided, it is not a stable workflow, which I am going to explain later. It is helpful if you are starting out an ARG or provide limited time public access to super secret confidential bullshit of exclusivity, this solution has you covered. Since these domains are not a super favorite for search engine crawlers, you have a super power to terminate the site, become MIA and pull a virtual neuralyzer over the hive mind we call the Internet.


#### Cons of Free domains, because you are smooth brain

1. Not at all serious shit

So basically you can register a free domain name from 4 select ccTLDs of countries like TK (Tokelau), GA (Gabon), CF (Central African Republic) and ML (Mali). You are only the user of the domain but have no legal right to it (they can revoke it at any time) - plus the whole 'its not a .com' thing.

2. DNS Propagation takes ages

‘DNS propagation’ is a term used to describe the time frame after making any changes to your domain name. DNS records are stored in cache mainly to improve the performance of DNS queries. Every DNS record has a Time to Live (TTL) value, which is the time DNS servers should store that record in the cache. Even if a record is changed, DNS servers will continue working with its formal value from cache until this time has passed.

This is the essence of DNS propagation – it is the time required for DNS servers worldwide to update their cached information for a domain name. It is influenced by the TTL of DNS records that might have changed, but there are also other factors that could come into play.

A DNS change requires up to 24 hours to propagate worldwide, although most often this happens in a matter of hours. But, in case of Freenom domains, it takes like a week. I don't know on what their root servers are running on.

3. Frequent outages for no apparent reason

One fine morning, you are in your sleepwear, started your computer to connect to the services, which you have painstakingly created by destroying most of your social life, with a cup of coffee; only to find that you are unable to connect. You login to your DNS provider to check if all routes are correct, and coming to the realization that the Root servers took a break and is no longer responding to DNS queries. Your morning DESTROYED and you receive an award - "Emotional Damage"


### My groundbreaking solution

__A Paid domain - [1bl4z3r.cyou](1bl4z3r.cyou)__

Why .cyou? Well, it was cheapest of all the domains present on Namecheap as on 30/06/2022. That's it, that's the only reason.

Now, let my Post-purchase Rationalization kick in and I over attribute positive features to the option I had chosen and negative features to options not chosen, thereby putting my choice as better than they actually were. 

#### Free Marketing Time


##### What is .cyou?

.cyou is a savvy and uber-cool domain extension crafted for the true digital natives of today.


##### Who is .cyou for?

The .cyou domain is crafted specifically for today’s generation of movers and shakers who have never lived in a world without social media or the internet.

.cyou empowers the true digital natives of today’s work smart generation that are re-writing the rule book on how things should be.

The .cyou domain is for the group of technologically advanced, Eco-conscious, open-minded individuals of today. It’s for Gen Z and for those who thrive on memes and speak fluent emoji.

From youngsters starting their own businesses to those who wish to monetize their passions and talents or for influencers who want to further their strong personal brands on social media, .cyou is for them all!

.cyou is also well-suited for any business or brand trying to appeal to Gen Z or simply looking for an innovative online identity that helps them stand out from the herd.

Simply put, .cyou is for any individual or business that shares the ethical values of diversity, inclusivity, and independence.

##### Agnostic and smart

The .cyou domain extension is not bound by any specific industry vertical or niche. Neither is it restricted by language or geographic boundaries. The agnostic nature of .cyou makes it usable by anyone from any industry from any part of the world.  

There are no usage limitations that come along with a .cyou domain extension. Anyone can use it to denote their online presence and build a strong brand around it. This makes .cyou a versatile and dynamic choice.    

The .cyou domain doubles up as a nifty abbreviation for ‘see you’.

##### Here's a logo, because uber-cool sites need uber-cool logo of a uber-cool domain

{{< figure src="images/buying-my-own-domain/cyou-domain-logo.webp" alt=".cyou Domain Logo" caption="Domain logo, because why not" loading="lazy" >}}

### What's next?

I liked the ml domain, but like my crushes, it is just a phase and I would eventually get over with it. For the time being, [1bl4z3r.ml](https://1bl4z3r.cyou) will redirect to [1bl4z3r.cyou](https://1bl4z3r.cyou).