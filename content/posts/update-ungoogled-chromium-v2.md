﻿---
title : 'Changes to ungoogled-chromium script' 
slug : 'update-ungoogled-chromium-v2' 
tags :
    - Privacy
    - Code
    - Web 
description : "ungoogled-chromium doesn't provide an update mechanism out-of-the-box, so I made one. This article focuses on the changes made in the script" 
date : 2023-04-24 
toc : true
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---

Hey there. The bot fam is seriously out of fodder these days from this little nook if the internet. I feel you bro. I am pretty wound up in various shits, which pays me. You have to understand that I started this site as a hobby project and I do not intend to apply it for monetization. I had momentarily weakness on applying as Brave Creator, however I had overcome that and double down on my intention to keep this site and all the sister pages ad-free. Which means there will be inconsistent article upload.
> BTW, this is my second post in 2023

This is a follow-up on my [previous article]({{< ref "update-ungoogled-chromium" >}}) detailing ungoogled-chromium updater script. **So let's gooooo....**

Direct Link to Download Script : [https://github.com/1bl4z3r/boredhub/blob/master/update-chromium.ps1](https://github.com/1bl4z3r/boredhub/blob/master/update-chromium.ps1)

## Major Changes

So, previously there wasn't any help function. Which seems out-of-character for a script or a program in the FOSS world. Henceforth, I have added help function. Here's how to invoke.

`Get-Help .\update-chromium.ps1`

```powershell
NAME
    update-chromium.ps1

SYNOPSIS
    Update (or download) ungoogled-chromium without hassle


SYNTAX
    update-chromium.ps1 [-silent] [-help] [<CommonParameters>]


DESCRIPTION
    This script allows you to update (or download) ungoogled-chromium on your Windows systems. This downloads binaries from official GitHub source
    and no modification is done at the supply chain.

    To give extra peace of mind, I developed this script in a Windows system where Administrator privilege is disabled, so this script doesn't ask
    the same; in addition that ungoogled-chromium is installed as normal user.

    Additional Links:
    GitHub : https://github.com/1bl4z3r/boredhub/blob/master/update-chromium.ps1
    Blog : https://1bl4z3r.cyou/posts/update-ungoogled-chromium-v2/

    Ungoogled-Chromium : https://github.com/ungoogled-software/ungoogled-chromium


RELATED LINKS
    https://1bl4z3r.cyou/posts/update-ungoogled-chromium-v2/

REMARKS
    To see the examples, type: "get-help update-chromium.ps1 -examples".
    For more information, type: "get-help update-chromium.ps1 -detailed".
    For technical information, type: "get-help update-chromium.ps1 -full".
    For online help, type: "get-help update-chromium.ps1 -online"
```

`Get-Help` Scriptlet, as its name suggests, provides Help for the script. All the contents for Help is present in the script itself, so it is self-contained.

### Some granular help

- For just examples :
`Get-Help .\update-chromium.ps1 -examples`

```powershell
NAME
    update-chromium.ps1

SYNOPSIS
    Update (or download) ungoogled-chromium without hassle
    -------------------------- EXAMPLE 1 -------------------------
    PS C:\>.\update-chromium.ps1 -silent

    Runs the Script silently without asking user confirmation
    -------------------------- EXAMPLE 2 -------------------------
    PS C:\>.\update-chromium.ps1 -help

    Display this Help message

    -------------------------- EXAMPLE 3 -------------------------
    PS C:\>help .\update-chromium.ps1 -online

    Check out Online help
```
- Get all the contents of Help message

`Get-Help .\update-chromium.ps1 -detailed`

```powershell
NAME
    update-chromium.ps1

SYNOPSIS
    Update (or download) ungoogled-chromium without hassle


SYNTAX
    update-chromium.ps1 [-silent] [-help] [<CommonParameters>]


DESCRIPTION
    This script allows you to update (or download) ungoogled-chromium on your Windows systems. This downloads binaries from official GitHub source
    and no modification is done at the supply chain.

    To give extra peace of mind, I developed this script in a Windows system where Administrator privilege is disabled, so this script doesn't ask
    the same; in addition that ungoogled-chromium is installed as normal user.

    Additional Links:
    GitHub : https://github.com/1bl4z3r/boredhub/blob/master/update-chromium.ps1
    Blog : https://1bl4z3r.cyou/posts/update-ungoogled-chromium-v2/

    Ungoogled-Chromium : https://github.com/ungoogled-software/ungoogled-chromium


PARAMETERS
    -silent [<SwitchParameter>]
        Specifies if script will ignore user confirmation. If provided it will not nag you for confirming each step.

    -help [<SwitchParameter>]
        Display Help message

    <CommonParameters>
        This cmdlet supports the common parameters: Verbose, Debug,
        ErrorAction, ErrorVariable, WarningAction, WarningVariable,
        OutBuffer, PipelineVariable, and OutVariable. For more information, see
        about_CommonParameters (https:/go.microsoft.com/fwlink/?LinkID=113216).


    PS C:\>.\update-chromium.ps1 -silent

    Runs the Script silently without asking user confirmation

    -------------------------- EXAMPLE 2 -------------------------
    PS C:\>.\update-chromium.ps1 -help

    Display this Help message

    -------------------------- EXAMPLE 3 -------------------------
    PS C:\>help .\update-chromium.ps1 -online

    Check out Online help

REMARKS
    To see the examples, type: "get-help update-chromium.ps1 -examples".
    For more information, type: "get-help update-chromium.ps1 -detailed".
    For technical information, type: "get-help update-chromium.ps1 -full".
    For online help, type: "get-help update-chromium.ps1 -online"
```

- For this online guide (Why?)
`Get-Help .\update-chromium.ps1 -online`

Another major change is use of `-silent` flag. It has come to my notice that some bots are trying to use this silently without user interaction, whilst other
bots were asking about user interaction. So, to satisfy both parties. I added silent flag, which when passed, will disable user interaction. It is helpful during automating tasks. In its default execution. You will be asked to confirm when closing Chromium windows and downloading latest version. You will be provided with following confirmation dialog.

```powershell
Confirm
Continue with this operation?
[Y] Yes  [A] Yes to All  [H] Halt Command  [S] Suspend  [?] Help (default is "Y"):
```
This is done by these additional lines of code. Don't worry, I will explain everything.

{{< highlight powershell "linenos=table" >}}
param ([switch] $silent)
function user-confirm($WarningMessage){
if($silent){
return
}else{
Write-Warning "$WarningMessage ?" -WarningAction Inquire
}
}
{{< / highlight >}}

`param` defines what are the parameters that can be supplied to the script while running. Parameters are like additional bits of info that can be supplied to change its behavior.  I have used `[switch]` type which doesn't hold any value in of itself. The presence of the switch is enough to tell the function what you want to do.

When its time to as user confirmation, the function `user-confirm` is run, which takes in a string `WarningMessage` that is passed to **`Write-Warning`**, which displays the Message and waits for user interaction, hence **`-WarningAction Inquire`**.

Following the same, since I am mostly from a Linux background. I have a habit of running `--help` after any command, which seems to slip out of my memory. For the Linux based bots out there like me, rejoice; as I happened to understand and implement an adapter for our old habits.

{{< highlight powershell "linenos=table" >}}
param ([switch] $help)
if($help){
Get-Help  -Name $PWD\update-chromium.ps1  -full;exit
}
{{< / highlight >}}

When run with `.\update-chromium.ps1 -help`, you will get same output as `Get-Help .\update-chromium.ps1 -full`. This relies on another Parameter called `$help`.

## Minor Changes

Some minor changes include colors to Text, provided by `-ForegroundColor ` property. I thought about adding `-BackgroundColor`, but let's not make it epilepsy inducing.

{{< highlight powershell "linenos=table" >}}
Write-Host  "We found that ungoogled-chromium is not installed"  -ForegroundColor 'red'
Write-Host  "Current version is $cur, which is outdated, hence downloading Latest Version: $ver"  -ForegroundColor 'red'
Write-Host  "Current version of ungoogled-chromium is $cur"  -ForegroundColor 'Magenta'
Write-Host  "Downloading latest version of ungoogled-chromium. Version: $ver"  -ForegroundColor 'Magenta'
Write-Host  "We found that ungoogled-chromium is installed"  -ForegroundColor 'cyan'
Write-Host  "Chromium is not running. We are good to go"  -ForegroundColor 'green'
Write-Host  "No new versions present, you are good to go"  -ForegroundColor 'green'
{{< / highlight >}}

---
That's all for now. I think this is the final form for the script, with little to no changes to be applied. If I find that something is breaking, I will fix that of course, but I am in my wits end if ay functionalities should be added.

This is where you bots come into picture. Suggest any changes you like to see. There is a [contact page]({{< ref "/contact" >}}), or message me in the myriad of Social Platforms.

This is 1BL4Z3R, Until we meet again

