---
title : 'Game called Life'
slug : 'game-called-life'
tags :
    - Personal
    - Thoughts
description : 'All the world’s a stage, And all the men and women merely players;'
date : 2022-04-01
toc : true
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---

```markdown
All the world’s a stage,
And all the men and women merely players;
They have their exits and their entrances;
And one man in his time plays many parts,
His acts being seven ages. At first the infant,
Mewling and puking in the nurse’s arms;
And then the whining school-boy, with his satchel
And shining morning face, creeping like snail
Unwillingly to school. And then the lover,
Sighing like furnace, with a woeful ballad
Made to his mistress’ eyebrow. Then a soldier,
Full of strange oaths, and bearded like the pard,
Jealous in honour, sudden and quick in quarrel,
Seeking the bubble reputation
Even in the cannon’s mouth. And then the justice,
In fair round belly with good capon lin’d,
With eyes severe and beard of formal cut,
Full of wise saws and modern instances;
And so he plays his part. The sixth age shifts
Into the lean and slipper’d pantaloon,
With spectacles on nose and pouch on side;
His youthful hose, well sav’d, a world too wide
For his shrunk shank; and his big manly voice,
Turning again toward childish treble, pipes
And whistles in his sound. Last scene of all,
That ends this strange eventful history,
Is second childishness and mere oblivion;
Sans teeth, sans eyes, sans taste, sans everything.
```


> from As You Like It, spoken by Jaques

It would be a lie if I said I didn't like the play `As You Like It`, well, I didn't like for the fact that I had to memorize every fucking line and understand deeper meaning of **What Mr. Shakespere really wanted to say?**, and write them down on a piece of paper in order to get grades and actually pass a Public Examination; but I still have this playbook (a simple token of the simpler lives).

So, why I am getting all teared up and nostalgic?

1. I have too much time in my hand
2. See above

I play chess. Mostly with a high ELO rated bot, and consecutively loose every time, which is to say I am not good with 0 Sum games. For my longest time, I was informed that life is also a 0 Sum game, ie. `If you don't win, you loose`. Simple enough for my teenage simple brain. But, what my folks forgot to mention that most often than not, the competitor whom you are trying to defeat is yourself.

{{< youtube 6SgEjPdeZGo >}}

That is a long-ass way to say, I got fucked (metaphorically) numerous times.

{{< figure src="images/game-called-life/chess.webp" alt="chess" caption="Chess-ify" class="center" >}}

## Life Lessons from Chess (coz I'm smart and didn't take an inspiration)

1. Every move has a purpose. Life obviously cannot be lived with this much unceasing calculation, nor should we want to live it that way, but there are times when we must align our actions with a predetermined strategy, instead of bumbling through it.

2. Play for the advantage. If you already have it, maintain it. If you don’t have it, seize it.

3. Everyone’s playing. Sometimes it’s a friendly, often it is more serious. The problem is that not everyone knows they’re playing – even after they have made a move.

4. Seize the initiative. If you wait around for someone else to make a decision for you, they will… and you probably won’t like how it turns out.

5. Learn to spot patterns. There are often clearly defined lines of success that work well. Learn to see these when they repeat, and take advantage of them.

6. Don’t get stuck on the formula. A little bit of creativity and lateral thinking can often take you to new heights.

7. Ignore what your opponent is trying to do at your own peril. We often get so absorbed in our own games and machinations that we ignore what is going on around us. Be aware of threats and alert to opportunities.

8. Simplify.

9. If you only play patzers, you never really improve – take on a few tough challenges, and even if you lose, try to learn something new.

10. Cut your losses. Sometimes you are going to lose material. Try to minimize your losses and move on.

11. Play the board, not the player. Don’t target your responses at people, target what they say and do. There is a difference.

12. Sometimes you get stuck in a position known in chess as zugzwang: where whichever move you make is a bad one. This is just the way it goes sometimes, in chess and in life.

13. There is nothing more satisfying than a discovered attack: Pretending to do one thing while attacking somewhere else. Learn to play and live less obviously and on more levels. This makes you less predictable and more interesting.

14. Be prepared to sacrifice material for position. Sometimes even the greatest material sacrifice can result in a winning position later on.

15. If you spend all of your time chasing lowly pawns, you may be on the receiving end of an opponent who cares less about small victories and more about winning the war.

16. A threat is best met with a move that improves your own position. Don’t get trapped into mindlessly trading moves and material in anger. Sometimes the solution is more gentle and cerebral.

17. You don’t have to be a devious swindler to win, you just have to be better.

18. We all blunder from time to time. This does not mean we should give up and run away. Often when you’re sure there is no way out after a bad mistake, you will be given a lifeline.

19. When someone makes a move that you cannot understand, don’t read more into it than you need to. Sometimes people just make silly moves – that’s all there is to it.

20. Have a Plan B. And a Plan C. If none of those work, you’re probably doomed.

21. Play for the middle. Don’t hold back too much, and don’t push through too early. Your opportunity will come.

22. How you start a game determines how you will finish it. Play wisely.

23. If an opening appears, seize it immediately.

24. Don’t get pinned down. Where something more cherished cannot be brought into play because it is stuck behind something trivial, make every effort to get it into the game – as soon as possible.

25. When you are in the final stretch, and about to win, anticipate what could go wrong and plan accordingly.

26. Be flexible. It seldom goes the way you planned – adjust and continue.

27. If you are feeling boxed-in, free things up.

28. Where possible, trade inferior material and positions for better ones.

29. The little guys on your side matter. Look after them.

30. Accumulate small advantages.

31. There are no foregone conclusions in life.

32. Ignore meaningless threats. Anticipate and deal with dangerous ones swiftly.

33. Never rest on your laurels. Keep thinking, looking for new opportunities and trying to generate new ideas.

34. Don’t be overly impressed with lofty words or titles. The only thing worse than being overly different towards those who outrank you is being dismissive of those inferior to you.

35. Keep calm and move slowly.

36. Replace wishful thinking with action.

37. If you lose, do so graciously and try to learn at least one important lesson.

38. Sometimes a draw is as good as a win. But a draw is always better than a loss.

39. Keep your options open and always have an escape route.

40. Surprise and impress people with unconventional moves. But not with dumb ones. Creativity always has a purpose – doing something wild and crazy just for the sake of it may be fun at the time, but ultimately has no value. Break the rules – but only if it serves a good purpose.

41. Appraise your position honestly. If it is bad, do something about it – if it is good, make it even better.

42. Don’t get swept up by distractions.

43. Narrow down your choices. And then decide. Take your time, but settle on one plan of action… and then do it!

44. Sometimes you have to sacrifice in order to achieve a break-through.

45. Always consider the whole board when deciding on a move: decisions made with too narrow a focus are often bad.

46. Connect your pieces cleverly. Collaboration and cooperation are the keys to success.

47. Look beyond the obvious.

48. Enjoy yourself.

49. Deep and meaningful is always better than superficially pretty.

50. If all else fails, fake it.

One more game which I often like to play is Tetris, just because it has fancy 8-bit-isq music; and no, I don't have enough patience to play Elden Ring, or any story based titles for that matter; which co-incidentally is more costly than former, but is just co-incidental (Trust me).

One thing of Teris, is that it is a simple game, and it have two objective - Clear the board and don't reach the celing. It is simplest game to make a auto-playing AI out of (If you don't consider Classic Tic-Tac-Toe and Rock-Paper-Scissors as a game). After meeting a special someone, my idea of life's a 0-Sum game had few flaws, namely it considers that I am not a source of fuck-up; which, if you meet me IRL, is absolutely not true (so was said to me).

{{< figure src="images/game-called-life/tetris.webp" alt="Tetris" caption="Tetris-ify" class="center" >}}

## Life Lessons from Tetris (coz, if I am going to increase watch time, I have to do "Random Bullshit, GO")

1. **Being “in the zone"**

Being in the zone is nothing more than achieving a heightened state of focus. This near-meditative state is not always easy to achieve. Figure out how to get there, then stay as long as you can.

2. **Most of the time you don’t get what you want**

You’ll get something that might work, and you’ll get something that doesn’t fit at all, and you’ll get three or four of the same worthless piece in a row. In real life this translates to settling for a sub-par apartment or ending up in a band whose music is not quite your cup of tea. But ya know what? It’s not the end of the world. Patience is a virtue, and the right piece eventually comes.

3. **Take what you can get, as you work towards what you want**

Sometimes it’s OK to get one or two lines, keeping everything manageable, while waiting for the 'piece'. You gotta do what ya gotta do to survive until you see your opportunity. Keep your end goals in mind, but don’t feel bad taking chump change along the way.

4. **You have to have faith, the Answer’s Right Around the Corner**

Ever had a moment in life where you had no idea how to handle things? A problem pops up, and you're at a loss for how to deal with it. Again, don't feel terrible! Everyone has these moments, and you can end up feeling a bit lost. Just because you can't see the solution now doesn't mean one isn't coming your way. All you have to do is keep your eyes open and contemplate all paths ahead. Good things are out there, but you must be patient. Sometimes when you're searching for the right answer, keep at it, have faith, and a perfect fit will come along and set things straight.

5. **You can’t rely on faith alone**

There is no way to predict when you’ll get it. Sure, the left side of the screen keeps tallys of each piece. But just like a display board on a roulette wheel, those numbers keep track of the past; they don’t predict the future. Mistakes are unavoidable. In order to progress in Tetris, sometimes you have to purposely make a mistake. Once those mistakes are made, they’re made. You can’t change them. You can’t fix them. You can only move forward, try to make better decisions and eventually those mistakes won’t matter. A little forethought can go a long way.

6. **Doing something is always better than doing nothing**

If you spend too much time thinking it over, and you miss out on both. Think about what you do, but DO SOMETHING. 

At the end of the day, we never truly know which decisions will be right or wrong, until we do them. So if you find yourself having trouble making a decision — just choose one. Chances are, even if you make a bad decision, you’ll learn from it and be able to make wiser decisions in the future.

7. **Sometimes an unconventional move is the way to go - but take calculated risks**

Having a routine or a method is great, but if the mood strikes you to go in a different direction, just go! Don’t be afraid to follow your instincts, even if everyone watching thinks it’s crazy.

However, it’s very easy to stay complacent in life. Once you make a handful of decisions, you can get in a place where mistakes are easy to avoid. If you want to go further though, then you have to make some risks at some point. In life, it’s smart to take risks, but those risks still have to be carefully thought through and if you don’t fully believe in that risk, then maybe tackle a few smaller risks first.

8. **The more you do it, the better you get**

Life continues to get faster. In Tetris, you’re given seven different blocks, in random patterns, and your goal is always the same — arrange them in such a way that will give you points. As the game gets faster, you don’t all of a sudden get an eighth block to arrange. The rules never change. It just gets faster.

In life, your goal is stay living. Make sure you have the food, water, and shelter you need to exist. Now of course, life is much more complicated than Tetris. As you get older, you will have to do things that you didn’t have to do before, BUT as a human, everybody’s got the same end goal — to try to live a fulfilling and happy life.

9. **The better you get, the tougher the challenges**

As you become better, people will give you tougher and tougher assignments. No one is asking you start on the 19th level. But if you’ve already gotten past the first 18, then why not try?

10. **Learn to ignore the music**

When you stack your pieces too high, the music in Tetris speeds up. This might create the illusion that the pieces are moving faster. They aren’t. Take heed and be careful, but don’t freak out. Worrying about the music only draws energy away from your focus. Know your deadlines, but don’t worry about them. Keep your objective in mind, and finish your task.

11. **Patience is a Virtue**

Take the early stages to learn a little patience; you'll be happy you did when things start getting fast and furious. It's easy to see how you can parlay your Tetris patience into real life. Sometimes you need to sit back and let things go at their own pace. Trying to speed them up might not work out, and getting angry about it isn't going to help a thing. Dial it back a notch, let things play out on their own, and be happy you didn't turn a slow-going situation into a high-speed disaster!

12. **Never Give Up**

No matter how bad things seem, no matter how much of a struggle you're facing, giving up isn't an option. Make sure to give everything you've got to accomplish the task ahead. More often than not, be it real-life or Tetris, you'll end up finding a way to come out the victor. What happens if you don't win? There's an important lesson to be learned there as well. You gave it your all and you never pulled back.

You've suffered a loss, but it's not the end of the world. Get back into the saddle and give things another shot! You can take inspiration from anyone, anything, and anywhere in life.

---

That's fucking good mantras to live life by, innit? Makes a butt-ton sense, *It's you vs you vs the world*, and only goal is to survive long enough that you aren't guilty for anything or perturbed what didn't able to acheive while in your deathbed. That's some pretty good equations, I'll give you that. But there's another way to look into this equation, where you are constantly chasing 'you' - Your dreams, desires and hope; and constantly running from 'you' - Your darkness, despair and guilt. Am I going somewhere with this? I have no fucking idea.

