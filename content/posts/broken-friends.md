﻿---
title : 'Broken Friends'
slug : 'broken-friends'
tags :
    - Personal
    - Thoughts
    - Random Access Memories
description : 'The conscious act of breaking friendships is actually lot more true'
date : 2018-08-06
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---

> **This post appeared in NoPurposeClub on August 6, 2018.** This is the beauty of taking backups...

5th August was Friendship Day, and I, bl4z3r, wishes you all a long-lasting and evergreen friendship on WordPress community and in real life. Though, I am behind a PC, and have no money in my pockets to do creative things “Behind the Mac”. So, sorry Apple fanboys/girls; But hey it works, and let’s keep it in that way.
From my own experience, I want to shed some Strobe Lights on the fact that like love, friends can break up, and there are no fallback to rulebooks regarding this.

25… That’s the number… When you start to loose friends. Sustaining relationships are always been a challenge in the Digital Life, but sustaining adult friendships a different league of hardness, which makes some of us loose our goodnight’s sleep. Our Facebook may have 99999 friends (_Ladies, yes I am talking to you!!_), which we could boast around in our timelines, but we are loosing friends in real life in the same rate too!!! (_Mic-Drop_)

Most of this come from one thing we need in life to sustain ourselves- a JOB. A demanding job puts an end to frequent get-togethers, that we used to crave in our college or university life. As someone focuses on climbing up the corporate ladder, priorities and values difference causes friction and cause old bonds to fall apart. Being of different industry, The ‘free time’ becomes nonexistent. Without both investing and compromising, the friends from childhood or, buddies of college fades away.

We loose our friendships and buddies at around 25, as we move out from college/universities to workplaces. Also, we make major life decisions for our life, one of which being LOVE. In everyone’s life, The Inner Core friends consists of at most five friends. Once, someone falls in love (or should I say- Kicked hard in ass by Cupid), that core of friends decreases to two.

The social media is the largest culprit of all of these. For reference, my father, my actual-sperm donator (_SpellCheck suggests- ‘detonator’_ :rofl:)-father, says such fables where his few friends did some pretty crazy stuffs together (_no, that didn’t involve drinking and clubs_), and always looks forward to meet face to face when someone from his group returns to city. He says the secret to his friendships maybe be the result of Zuckerberg not being born. 

That’s it. We are so wired up in wireless mode of communication, that we simply couldn’t move out from our comfort zone to meet new faces in flesh, and do all the crazy stuffs. Facebook, Instagram etc. gives us a false notation that our friends are close to us, as we get their timely status and make the silliest comment on them (_BTW, I make the silliest comment; FYI_). Due to this, we don’t make an effort to meet them. Before social media, meeting friends were crucial parts of our lives.

Offline relations takes more energy, time, effort and understanding. Physical presence of each other, and interplay of emotions is real, with close to impossible chances to mask them. It helps to relax and be ourselves, without the burden to build up our online avatar.

A final advise to the readers from the individual, who understands the value of friendships, by loosing it altogether… **KEEP YOUR FRIENDS CLOSE. Period**

Friends become very crucial for our mental and physical health as we grow old. Indeed, families are important, but it burdens with duties and responsibilities that tend to drain us. With friends, we mostly pursue activities and passions which make us happy, and be free with all the firewalls turned down.

Keep strong, and yet again, belated Happy Friendship Day.
Don’t be:-

```markdown
We’re not friends, but strangers with memories.
```

---
COMMENTS

> There’s a lot of reality in this post!
Thanks for the reminder and thanks for sharing, my friend! - Shreya Vohra [https://phoenixwithapen.wordpress.com](https://phoenixwithapen.wordpress.com)
>> Sure thing.

> I ended up with a heavy heart after reading this post - lemon head [https://bidisha752301104.wordpress.com](https://bidisha752301104.wordpress.com)
>> I am sorry to make your heavy heart. But it is the eternal truth