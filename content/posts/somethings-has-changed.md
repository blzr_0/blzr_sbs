---
title : 'Somethings has changed'
slug : 'somethings-has-changed'
tags :
    - Personal
    - Web
    - Thoughts
description : "Well, somethings has changed in my personal life, which I am proud of"
date : 2023-07-29
toc : false
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---

Heeya bots, "long time no see", you may ask!!
Well, somethings has changed and I am excited to tell all about it.

No, I did not romantically entangled with a human female. Not that I detest romantically entangling with a human female, but considering the political landscape where I might be kicked out of job and land in a prison; if a human female decides just to **Have Fun**. The risk is far too greater for the reward.

I am talking about my second love, that is.... Computers. Well, we all knew that.

So, it might be a long article. Bear with me.

First thing that happened was GitHub decided to block my account for public access with no prior communication. Since this site is hosted on GitHub Pages, I was fucked. Though I made an appeal to reinstate my account, I realized that the proverb is true.

{{< admonition warning >}}
Never put all your eggs in one basket
{{< /admonition >}}

Hence, searching for a good alternative, I landed on GitLab. It has its own restrictions, sure; but, I can be assured that my account won't freeze up out of the blue.

### So 1bl4z3r, how are you using GitLab to serve up your site?

Glad you asked. Well... not glad, coz I asked myself and you are reading.

GitLab has this thing called GitLab Pages (very original, I know). Only difference I could even think of is the complete autonomy on the SSG or, [Static Site Generators](https://jamstack.org/generators). So you are not locked in. Additionally, I am invested in DevOps, so it is a great playground to learn about pipelines.

To be honest, I didn't have much extensive knowledge on GitLab's pipelines, hence previous paragraph. I had cobbled together a simple script.

{{< highlight yaml "linenos=table" >}}
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Pages/Hugo.gitlab-ci.yml
# All available Hugo versions are listed here:
# https://gitlab.com/pages/hugo/container_registry

default:
  image: "${CI_TEMPLATE_REGISTRY_HOST}/pages/hugo/hugo_extended:latest"

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
    - hugo
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

pages:
  script:
    - hugo
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  environment: production
{{< /highlight >}}

That the most simplest Pipeline, according to the Internet. But, I got score close to 90, I don't think I could ask for more. But, I agree, that there is always room for improvements. 

{{< figure src="images/somethings-has-changed/home-page-result.webp" alt="Home Page Score" caption="Home Page Score" loading="lazy" >}}

{{< figure src="images/somethings-has-changed/posts-page-result.webp" alt="Posts Page Score" caption="Posts Page Score" loading="lazy" >}}

I will slowly move all my public projects from GitHub to GitLab; I am able to do some makeover and polish it up. I intend to have a dedicated page here, so that I can make a projects portfolio. That was long thing pending.

---
Next update for me is....

### I am finally maintaining an Open Source Project

Let me give you a backstory.

I was using Jekyll for a long time as a SSG. It was good, no doubt, but it lacks customization. It was good when articles count is small but completely breaks apart in fast loading; with me getting >50 points in Lighthouse. After much debating and procrastination, I decided to switch to Hugo, getting wooed by its performance. If you didn't know about Hugo, [here's the link ](https://gohugo.io/).

This means, I need a theme, just because I'm not creative enough to make a theme on my own. I had to search far and wide to get a theme that works for me and after a week's worth of searching, I got attracted to [Hermit](https://github.com/Track3/hermit). It was minimal enough to my liking and fast enough so that I can be proud of. If you are reading this article, it would be fair to say that you feel same way too. There are myriad of reasons why I selected Hermit. And you guessed it, I will list it :

1. It is using dark mode by default
2. The dependencies for CSS and JS is minimal; with all the core files being merged into one.
3. It is fast, I told that before
4. No third party framework, this was a big win for me

But as luck would have it, the original developer stopped working on it, and consequently it was removed from Hugo Themes page, as being unmaintained for more than 2 years. That was the story in [August, 2021](https://github.com/Track3/hermit/issues/160). I saw this issue on [June, 2023](https://github.com/Track3/hermit/issues/160#issuecomment-1598340298). So, since I am using the theme for blog, I though to myself 

> Why not maintain this theme too; what's the harm?

So, I have started maintaining this theme, with a new, totally original name `Hermit - V2`. After every couple of weeks, I get time to sit in front of my Development PC, to look into Open Issues and Un-merged Pull Requests, and doing my best to decide what goes with or against the very spirit of Original Author's vision.

Some of the changes I had added are :

- homeSubtitlePrinter : To have a printer-like effect on Home Subtitle
- Added Mastodon Icon
- Added support for last modification date
- Override Figure Shortcode to support lazyloading

However, there are some things which I believe that goes against Open Source in general, that is anything related with money. In [my own words]({{< ref "update-ungoogled-chromium-v2.md" >}}) of my own [blog](https://blzr.sbs), I told that I will never support ads.

{{< admonition info "[My Words]" >}}
You have to understand that I started this site as a hobby project and I do not intend to apply it for monetization. I had momentarily weakness on applying as Brave Creator, however I had overcome that and double down on my intention to keep this site and all the sister pages ad-free.
{{< /admonition >}}

This is not a goodbye. I am in the middle of something big. I will let you bots know when I have a clear head.