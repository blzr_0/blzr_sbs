---
title : 'Things I wish I knew at 21'
slug : 'things-i-wish-i-knew-at-21'
tags :
    - Personal
    - Thoughts
    - Random Access Memories
    - Resource
description : 'Things I know at 41, I wish I had known at 21'
date : 2024-03-01
toc : false
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---

### The biggest difference between success & failure is getting started:
The majority of people I know fantasize about things that actually can be accomplished. They just never get started.

If you get started and play the long game, you have a great chance of winning.

### The 2nd biggest difference between success & failure is persistence:
Successful friends & peers of mine have almost always been doing their "thing" for decades. Not years.

Most people give up in the "I suck at this" phase without considering the journey from a 3Ok ft view.

### 99% of people are out for themselves, even if it doesn't appear so:
Most people might find this pessimistic, but I just find it realistic.

Curious about someone's motivation? Look at how they are incentivized.

### It's difficult to build a work ethic without the right environment:
Everyone talks about "hard work" and "outworking the competition". It's tough to forcefully create this behavior.

Curate your environment and the behavior is a more likely outcome.

### Throw your 10-year plan in the trash:
When I look at the people in my network who are on top, most have embraced pivots, chance, and randomness. They had a plan. They had milestones.

They changed them when data (and qualitative feedback) suggested doing so.

### Getting 1% better at something means leaping millions:

1% of the total internet-connected population is 46M people.

Continue to get 1% better each month, quarter, or year, and you're making significant strides in the overall ecosystem.

### Comparing yourself to others is the easiest way to get distracted:
You can only control how successful you are, not how successful someone else is.

Be the best version of yourself, not a better version of someone else.

### Everyone has knowledge that other people will pay for:
The idea that you have to be an "expert" or have a certificate to teach or sell something is a fallacy.

Expertise is linear. You can make a living helping those 2-3 steps earlier in the journey than you.

### There is absolutely nothing wrong with being selfish:
Can you be selfish to a fault? Of course.

If you always put the needs of others over yours (and your families), life will be more difficult. 

Give and take.

Or get taken advantage of.

### If you can't teach yourself, you're a liability:
If people have to spend their time teaching you simple things that you can learn on the internet, you'll always be considered a liability.

It doesn't matter what you do for a living. Being auto-didactic is a differentiator.

### The most important skill to learn is how to learn on your own:
Mentors are great, but they are overrated.

Mentors work well when you've reached a ceiling of teaching yourself everything you could possibly know about a subject.

Learn to learn and you're unstoppable.

### You won't do anything special without a little risk:
Those at the top of their fields nearly always took some risk.
- Risky idea.
- Risky execution.
- Risky go-to-market.
- Risky advertising plan.

Don't be catastrophically foolish. But do take some chances.

### Money is not the root of all evil:
Money is simply the key to unlocking whomever you are as a person. 

Get your hands on some money so you can be the best version of yourself. Help other good people get theirs too.

Bad people with money are what's evil.

### Consistency trumps all traits:
There are lots of skills you can learn. But as a pure trait? Consistency is tough to beat.

Athlete, businessperson, creator, etc. Do your thing every single day and reap the rewards downstream.

### Nearly everyone is winging it:
Are some people light-years ahead when it comes to their thing? Of course. Elon, Steve Jobs, etc.

But most people who you admire are still figuring it out. Every day, just plugging along. As nervous as you are. Find comfort in that.

### It is ok to not feel connected anymore with your school/college friends.

It is better to acknowledge it and express it, than to slowly drift apart and live with the guilt "did I do it right?"

### The world will do everything to dictate a timeline for your life.

Finish studies by 24.
Get married by 27.
Kids by 30.
If you listen to the world, you will live their life. Not yours.

### You do not have to be an extrovert to win.

Extroversion/Introversion has nothing to do with people. Instead it is how you gain energy. From people, or from within.

If you are introvert, strengthen yourself instead of wanting to change yourself.

### Most life plans do not work out. And that's part of the plan!

Things will change.
You will change.
The world will change.
And thus your plans will change.

When they do, embrace the new you.
It is telling you something.

### It is the only decade where

- you are big enough to take big risks, and
- have a life long enough to recover if those risks do not work out

Take risks.
You will be surprised at how many of them
work out. Or how easily you get over them.

### No one expects you to be sorted.

But you will believe you are the only one who is not!

The most binding trait of all those in their 20s, is that all of them are trying to make sense of their life.
No one has it figured.

### Everyone in their 20s thinks they are too old.

But you are not.
You are really really early!
You are just getting started.
You have time to figure things out.
The pressure of time is self imposed.

### Life will move really fast. Don't let it.

Do not live your life as if its happening to you. Eat. Work. Sleep. Repeat.

Pursue hobbies, learn, connect with people, work out, read, exercise, meditate, think, wake up / sleep on time, do things outside of work. LIVE LIFE!

### Don't make money to spend. Make it to retire.

If you earn to spend, you will be forced to earn all your life. If you earn to free yourself from earning, you will be able to do whatever you want to do.

For the rest of your life!

### You are responsible ONLY for your happiness. Not that of others.

Other people's baggage is theirs to deal with.
Acknowledge them.
Empathize with them.
Support them.
But allow them to work things out on their own.

### FOMO will always lead to short term decisions

True for money.
True for career.
True for relationships.
True for life!

### If people like you, life will be much easier.

Be kind.
Be generous.
Compliment people.
Help people.
Do it for no returns.
And you will get massive returns!

### Be aware of what you are feeling

20s is when we are feeling the most. Because so much around us is constantly changing.

Don't try to control your emotions. Instead be aware of them.

Why am I angry / sad / scared / excited / happy / nervous? Make your mind your friend. Not enemy.

### Your goals are merely desires

Goals are weird. We hit them - feel great. We don't hit them - feel shitty. But when we set them, there is no basis for it. Only a desire. Don't set goals. Set habits.

And they will take you to far great goals you could have set for yourself.

### Talking about people will never take you far. Talking about ideas will.

Surround yourself with people who talk ideas. Take yourself out from circles where you are simply talking about people!

### A person isn't nice because they are nice to you.

They are nice because they are nice to everyone, especially those they don't need to be nice to!

Notice yourself when you start falling for people, just because they are nice to you! Observe how they treat everyone.

### Keep asking questions, even if you think it annoys people

Only those who ask questions move forward. Rest everyone accepts the answer society has given them!

### Your self-esteem is yours to nurture. Not for others to define.

But others will define it for you. And you will outsource your self-esteem to them. Until the day they leave and you will be left with no self-esteem.

Love yourself.
Respect yourself.
Nurture yourself.

### Don't optimize for money. Optimize for learning.
This decade: If you learn at the expense of money, you will earn a lot more in the future than you imagined.

If you earn at the expense of learning, you will have to keep earning and learning for the rest of your life!

### If you abuse your body, you wouldn't realize the harm until much later.
Don't treat your body as a waste bin. Don't ignore the signs. Don't postpone healing yourself to tomorrow.

You shouldn't need an accident to tell you that you are damaged.