---
title : 'The Most Powerful Ideas'
slug : 'most-powerful-ideas'
tags :
    - Personal
    - Thoughts
    - Random Access Memories
    - Resource
description : '20 of the most important ideas everyone should know'
date : 2024-03-15
toc : true
scrolltotop : true
ShowLastmod : false
Lastmod : 0000
---

### Cunningham's Law:
The best way to find the right answer on the internet is not to ask the right question, but to post the wrong answer.

Why? Because people are more interested in criticizing others than helping them.

### The Streisand Effect:
In some cases, an effort to kill an idea can lead to it becoming more popular instead.

Banned books and music albums that end up becoming popular precisely because they were banned are the most famous examples of this effect.

### Dunning-Kruger Effect:
People with little knowledge of a field will often overestimate their competence compared to more experienced players.

American Idol was a great example of amateurs who overestimated their ability to sing.

### The Lindy Effect:
The life of perishable things, like food, decreases with age. But the life of non-perishable things, like ideas, increases with age.

The ideas that are most likely to exist 1,000 years from now are the ideas that have already existed for 1,000 years.

### Confirmation bias:
We demand extraordinarily strong evidence for ideas that do not align with our beliefs, while accepting extraordinarily weak evidence for ideas that do.

### Hick's Law:
The effort required to make a decision increases with the number of options.

The more options you offer, the harder it is for customers to decide. This is why most companies are now creating products with fewer options.

### Brandolini's Law:
The amount of effort required to debunk misinformation is orders of magnitudes higher than the amount of effort required to create it.

This is why misinformation is so widely spread on the internet.

### Cultural Parasitism:
The ideas and beliefs that are most widely spread in society are the ones that are most likely to be transmitted to others - not the ones that are most likely to be true.

Fake news is an example of an idea that spreads very quickly despite being false.

### Luxury Beliefs:
Beliefs that confer status on the upper class while inflicting costs on the lower class.

Example: elites who support the abolition of police while living in privately guarded communities themselves, leaving poorer neighbourhoods to suffer the consequences.

### Decoy Effect:
Asymmetric numbers can influence our perception of what is acceptable. This is how cinemas sell more popcorn.

By artificially increasing the price of the middle option, they make the largest option the most attractive.

### Incentive bias:
Strong incentives can cause people to adopt beliefs that are incorrect or false.

> "It is difficult to get a man to understand something when his salary depends upon his not understanding it."

### Reciprocity bias:
We feel obliged to repay people who have done us a favor.

Example: When waiters gift free mints with the bill, customer tips go up by 14%.

### Mimetic Desire:
People have a desire to be more like their role models by copying them. This is why athletes like LeBron and Ronaldo are paid so much to wear Nikes.

People think they can be more like their role models by wearing what their idols are wearing.

### Risk Aversion
We often hesitate to purchase things because we hate the thought of buying something and regretting it later.

This is why companies like Netflix offer free trials - to counter our risk aversion.

### Uncertainty Aversion:
People are more bothered by the uncertainty of a wait than the duration.

This is why you see countdown clocks at traffic signals. It's also why your food delivery app gives you an estimated time for your food to arrive.

### Social Proof:
When people don't know how to act, they will blindly copy what everyone else is doing.

This is how smoking spread among women in the US - advertisers paid female models to smoke at public events.

### Fredkin's Paradox:
The more similar two options are, the more difficult it is to decide between the two.

You will soend much more time deciding between a Honda and a Toyota, than you will deciding between a Honda and a Ferrari.

### Status-seeking:
When choosing between 2 options, people will often pick the one that enhances their social status - even if that option is very costly.

Example: People will pay $2,500 for a branded handbag, even though its functionality is identical to a $250 bag.

### Evolutionary Mismatch:
Humans evolved in scarcity but now live in abundance. This makes it difficult for us to resist things that are abundant today but scarce in the past - like sugar and drugs.

This evolutionary mismatch causes many problems like obesity and addiction.

### Metcalfe's Law:
The value of a network increases as the number of users in that network increases.

This is how social media works: the more friends you have using an app, the more likely you are to join and use that app.