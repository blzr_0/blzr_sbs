---
title: "Contact"
date: 2023-11-15T16:28:34+05:30
draft: false
custom_css : 
    - "contact/contact.scss"
custom_js : 
    - "contact/contact.js"
---

“Communication - the human connection - is the key to personal and career success.”
> Paul J. Meyer

If you want to say hello! Best & quick way to do so is though below social and email links. Since the world isn't a nice place, please click on the handles on the right to reveal the links   :smiling_face_with_three_hearts: :wave:

{{< contact >}}

---

Or, if you prefer a formal approach, here's how to do that :technologist: :arrow_down_small:
{{< admonition failure "Critical Failure" >}}

{{< /admonition >}}
{{< contact section=form >}}