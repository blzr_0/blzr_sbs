const emaill = document.getElementById("email-link");
const tw = document.getElementById("twitter-link");
const li = document.getElementById("linkedin-link");
const contactForm = document.getElementById("contact-form");
const captchaText = document.getElementById("captchaText");
const messageText = document.getElementById("message");
const submit = document.getElementById("submit");
const admonition = document.getElementsByClassName("admonition")[0];
const errorMessage = document.getElementsByClassName("admonition-content")[0];
const alphaNums = "ABCDEFGHKLMNPQRSTUVWXYZabcdefghkmnpqrstuvwxyz23456789!@#$%^&({[<>]})?";
var generatedCaptcha = '';
var sent = false;
var url = "https://worker-to-telegram.workerscf.workers.dev/submit";
var request = new XMLHttpRequest();
contactForm.classList.remove("formdisable");
function randomColor() {
	let r = Math.floor(Math.random() * 256);
	let g = Math.floor(Math.random() * 256);
	let b = Math.floor(Math.random() * 256);
	return 'rgb(' + r + ',' + g + ',' + b + ')';
}

function generateCaptcha() {
	if (!sent) {
		const captchaCanvas = document.getElementById("captchaCanvas");
		const ctx = captchaCanvas.getContext("2d");
		generatedCaptcha = '';
		captchaText.value = '';
		ctx.font = "25px Roboto";
		ctx.letterSpacing = "20px";
		ctx.clearRect(0, 0, captchaCanvas.width, captchaCanvas.height);
		ctx.beginPath();
		for (let i = 1; i <= 7; i++) {
			var cText = alphaNums.charAt(Math.random() * alphaNums.length);
			generatedCaptcha += cText;
			let sDeg = (Math.random() * 30 * Math.PI) / 180;
			let x = 10 + i * 20;
			let y = 20 + Math.random() * 8;
			ctx.translate(x, y);
			ctx.rotate(sDeg);
			ctx.fillStyle = randomColor();
			ctx.fillText(cText, captchaCanvas.width / 6, captchaCanvas.height / 10);
			ctx.rotate(-sDeg);
			ctx.translate(-x, -y);
		}
		for (let i = 0; i <= 6; i++) {
			ctx.strokeStyle = randomColor();
			ctx.beginPath();
			ctx.moveTo(
				Math.random() * captchaCanvas.width,
				Math.random() * captchaCanvas.height
			);
			ctx.lineTo(
				Math.random() * captchaCanvas.height,
				Math.random() * captchaCanvas.height
			);
			ctx.stroke();
		}
		for (let i = 0; i < 50; i++) {
			ctx.strokeStyle = randomColor();
			ctx.beginPath();
			let x = Math.random() * captchaCanvas.width;
			let y = Math.random() * captchaCanvas.height;
			ctx.moveTo(x, y);
			ctx.lineTo(x + 1, y + 1);
			ctx.stroke();
		}
	}
}
generateCaptcha();

const sendMessage = () => {
	admonition.style.display = "none";
	var message = messageText.value;
	var enteredCaptcha = document.getElementById("captchaText").value;
	if (!sent) {
		if (enteredCaptcha === generatedCaptcha) {
			if (message.length >= 20) {
				sent = true;
				var today = new Date();
				var dateTime = (today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear()) + ' ' + (today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds());

				captchaText.disabled = true;
				messageText.disabled = true;
				submit.disabled = true;
				var messageJson = JSON.stringify({
					MSG: `${message}`,
					UA: `${navigator.userAgent}`,
					LANG: `${navigator.language}`,
					DT: `${dateTime}`,
					ZONE: `${Intl.DateTimeFormat().resolvedOptions().timeZone}`
				});
				request.open("POST", url, true);
				request.setRequestHeader("Content-Type", "application/json");
				request.onreadystatechange = function () {
					if (request.readyState === 4 && request.status === 200) {
						console.log(JSON.parse(request.response));
					}
				};
				request.send(messageJson);
				submit.innerHTML = "Sent &#10003;";
			} else {
				admonition.style.display = "block";
				errorMessage.innerHTML = "Message length must be greater than 20 characters";
			}
		} else {
			admonition.style.display = "block";
			errorMessage.innerHTML = "Invalid or wrong Verification Code";
		}
	}
}
emaill.onclick = () => {
	emaill.classList.remove("prevent");
	const emtx = `<a href="mailto:contact@blzr.sbs?subject=Contact%20BLZR%20-%20Opportunity%20%2F%20Casual%20Contact">contact@blzr.sbs</a>`
	emaill.innerHTML = `${emtx}`;
};
tw.onclick = () => {
	tw.classList.remove("prevent");
	const twtx = `<a href="https://twitter.com/messages/compose?recipient_id=247703829&text=Hi BLZR, I'm" data-screen-name="@BLZR_0">@BLZR_0</a>`
	tw.innerHTML = `${twtx}`;
};
li.onclick = () => {
	li.classList.remove("prevent");
	const litx = `<a href="https://www.linkedin.com/in/ristik-majumdar/">ristik-majumdar</a>`
	li.innerHTML = `${litx}`;
};